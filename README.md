# python教程2.x笔记

#### 项目介绍
本项目记录了python2.x版本的学习笔记，入门部分来源于菜鸟教程网站的学习。

笔记中只会记录教程中比较重要的一些内容，一些不是那么重要的内容会省略掉。

 **请注意，本笔记基于python2.x版本。** 

## Python 基础教程
第一个python程序：
```py
#!/usr/bin/python
print "Hello, World!";
```
### Python 简介
* Python 是一种解释型语言： 这意味着开发过程中没有了编译这个环节。类似于PHP和Perl语言。
* Python 是交互式语言： 这意味着，您可以在一个Python提示符，直接互动执行写你的程序。
* Python 是面向对象语言: 这意味着Python支持面向对象的风格或代码封装在对象的编程技术。
* Python 是初学者的语言：Python 对初级程序员而言，是一种伟大的语言，它支持广泛的应用程序开发，从简单的文字处理到 WWW 浏览器再到游戏。

Python 是由 Guido van Rossum 在八十年代末和九十年代初，在荷兰国家数学和计算机科学研究所设计出来的。

#### python的特点
* 1.易于学习：Python有相对较少的关键字，结构简单，和一个明确定义的语法，学习起来更加简单。
* 2.易于阅读：Python代码定义的更清晰。
* 3.易于维护：Python的成功在于它的源代码是相当容易维护的。
* 4.一个广泛的标准库：Python的最大的优势之一是丰富的库，跨平台的，在UNIX，Windows和Macintosh兼容很好。
* 5.互动模式：互动模式的支持，您可以从终端输入执行代码并获得结果的语言，互动的测试和调试代码片断。
* 6.可移植：基于其开放源代码的特性，Python已经被移植（也就是使其工作）到许多平台。
* 7.可扩展：如果你需要一段运行很快的关键代码，或者是想要编写一些不愿开放的算法，你可以使用C或C++完成那部分程序，然后从你的Python程序中调用。
* 8.数据库：Python提供所有主要的商业数据库的接口。
* 9.GUI编程：Python支持GUI可以创建和移植到许多系统调用。
* 10.可嵌入: 你可以将Python嵌入到C/C++程序，让你的程序的用户获得"脚本化"的能力。

### Python 环境搭建
#### python的下载
* Python官网：https://www.python.org/
* Python文档下载地址：https://www.python.org/doc/

python的安装很简单，就不说了，这里建议下载msi的下载包，这样可以自动配置一些东西比如环境变量什么的。
#### 在 Windows 设置环境变量
在环境变量中添加Python目录：

在命令提示框中(cmd) : 输入 
```
path=%path%;C:\Python
```
按下"Enter"。

 **注意: C:\Python 是Python的安装目录。** 

#### Python 环境变量
下面几个重要的环境变量，它应用于Python：
<table>
<tbody><tr><th>变量名</th><th>描述</th></tr>
<tr><td>PYTHONPATH</td><td> PYTHONPATH是Python搜索路径，默认我们import的模块都会从PYTHONPATH里面寻找。</td></tr>
<tr><td>PYTHONSTARTUP </td><td>Python启动后，先寻找PYTHONSTARTUP环境变量，然后执行此变量指定的文件中的代码。</td></tr>
<tr><td>PYTHONCASEOK </td><td>加入PYTHONCASEOK的环境变量, 就会使python导入模块的时候不区分大小写.</td></tr>
<tr><td>PYTHONHOME </td><td>另一种模块搜索路径。它通常内嵌于的PYTHONSTARTUP或PYTHONPATH目录中，使得两个模块库更容易切换。</td></tr>
</tbody></table>

#### 集成开发环境（IDE：Integrated Development Environment）
推荐：PyCharm

PyCharm 下载地址 : https://www.jetbrains.com/pycharm/download/

### Python 中文编码
Python中默认的编码格式是 ASCII 格式，在没修改编码格式时无法正确打印汉字，所以在读取中文时会报错。

解决方法为只要在文件开头加入 `# -*- coding: UTF-8 -*-` 或者 `#coding=utf-8` 就行了

注意：`#coding=utf-8` 的 `=` 号两边不要空格。

注意：Python3.X 源码文件默认使用utf-8编码，所以可以正常解析中文，无需指定 UTF-8 编码。

注意：如果你使用编辑器，同时需要设置 py 文件存储的格式为 UTF-8
### Python 基础语法
可以使用交互式编程或脚本式编程。
#### Python 标识符
在 Python 里，标识符由字母、数字、下划线组成。

在 Python 中，所有标识符可以包括英文、数字以及下划线(_)，但不能以数字开头。

Python 中的标识符是区分大小写的。

以下划线开头的标识符是有特殊意义的。以单下划线开头 `_foo` 的代表不能直接访问的类属性，需通过类提供的接口进行访问，不能用 `from xxx import *` 而导入；

以双下划线开头的 `__foo` 代表类的私有成员；以双下划线开头和结尾的 `__foo__` 代表 Python 里特殊方法专用的标识，如 `__init__()` 代表类的构造函数。

Python 可以同一行显示多条语句，方法是用分号 `;` 分开
#### 行和缩进
学习 Python 与其他语言最大的区别就是，Python 的代码块不使用大括号 {} 来控制类，函数以及其他逻辑判断。python 最具特色的就是用缩进来写模块。

缩进的空白数量是可变的，但是所有代码块语句必须包含相同的缩进空白数量，这个必须严格执行。
#### 多行语句
Python语句中一般以新行作为语句的结束符。

但是我们可以使用斜杠（ \）将一行的语句分为多行显示，如下所示：
```py
total = item_one + \
        item_two + \
        item_three
```
语句中包含 [], {} 或 () 括号就不需要使用多行连接符。如下实例：
```py
days = ['Monday', 'Tuesday', 'Wednesday',
        'Thursday', 'Friday']
```
#### Python 引号
Python 可以使用引号( `'` )、双引号( `"` )、三引号( `'''` 或 `"""` ) 来表示字符串，引号的开始与结束必须的相同类型的。

其中三引号可以由多行组成，编写多行文本的快捷语法，常用于文档字符串，在文件的特定地点，被当做注释。
```py
word = 'word'
sentence = "这是一个句子。"
paragraph = """这是一个段落。
包含了多个语句"""
```
#### Python注释
python中单行注释采用 `#` 开头。
python 中多行注释使用三个单引号(`'''`)或三个双引号(`"""`)
#### Python空行
空行与代码缩进不同，空行并不是Python语法的一部分。书写时不插入空行，Python解释器运行也不会出错。但是空行的作用在于分隔两段不同功能或含义的代码，便于日后代码的维护或重构。
#### 等待用户输入
下面的程序执行后就会等待用户输入，按回车键后就会退出：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

raw_input("按下 enter 键退出，其他任意键显示...\n")
```
以上代码中 ，`\n` 实现换行。一旦用户按下 enter(回车) 键退出，其它键显示。
#### 同一行显示多条语句
Python可以在同一行中使用多条语句，语句之间使用分号(;)分割.
#### Print 输出
print 默认输出是换行的，如果要实现不换行需要在变量末尾加上逗号 `,`
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

x="a"
y="b"
# 换行输出
print x
print y

print '---------'
# 不换行输出
print x,
print y,

# 不换行输出
print x,y
```
#### 命令行参数
很多程序可以执行一些操作来查看一些基本信息，Python 可以使用 `-h` 参数查看各参数帮助信息：
```py
$ python -h 
usage: python [option] ... [-c cmd | -m mod | file | -] [arg] ... 
Options and arguments (and corresponding environment variables): 
-c cmd : program passed in as string (terminates option list) 
-d     : debug output from parser (also PYTHONDEBUG=x) 
-E     : ignore environment variables (such as PYTHONPATH) 
-h     : print this help message and exit 
 
[ etc. ]
```
### Python 变量类型
变量存储在内存中的值。这就意味着在创建变量时会在内存中开辟一个空间。

基于变量的数据类型，解释器会分配指定内存，并决定什么数据可以被存储在内存中。

因此，变量可以指定不同的数据类型，这些变量可以存储整数，小数或字符。 
#### 变量赋值
Python 中的变量赋值不需要类型声明。

每个变量在内存中创建，都包括变量的标识，名称和数据这些信息。

每个变量在使用前都必须赋值，变量赋值以后该变量才会被创建。

等号（=）用来给变量赋值。

等号（=）运算符左边是一个变量名,等号（=）运算符右边是存储在变量中的值。
#### 多个变量赋值
Python允许你同时为多个变量赋值。例如：
```py
a = b = c = 1
```
以上实例，创建一个整型对象，值为1，三个变量被分配到相同的内存空间上。

您也可以为多个对象指定多个变量。例如：
```py
a, b, c = 1, 2, "john"
```
以上实例，两个整型对象1和2的分配给变量 a 和 b，字符串对象 "john" 分配给变量 c。
#### 标准数据类型
Python有五个标准的数据类型：
* Numbers（数字）
* String（字符串）
* List（列表）
* Tuple（元组）
* Dictionary（字典）
#### Python数字
您也可以使用del语句删除一些对象的引用。
del语句的语法是：
```py
del var1[,var2[,var3[....,varN]]]]
```
您可以通过使用del语句删除单个或多个对象的引用。例如：
```py
del var
del var_a, var_b
```
Python支持四种不同的数字类型：
* int（有符号整型）
* long（长整型[也可以代表八进制和十六进制]）
* float（浮点型）
* complex（复数）
#### Python字符串
字符串或串(String)是由数字、字母、下划线组成的一串字符。

python的字串列表有2种取值顺序:

从左到右索引默认0开始的，最大范围是字符串长度少1

从右到左索引默认-1开始的，最大范围是字符串开头

如果你要实现从字符串中获取一段子字符串的话，可以使用变量 [头下标:尾下标]，就可以截取相应的字符串，其中下标是从 0 开始算起，可以是正数或负数，下标可以为空表示取到头或尾。

当使用以冒号分隔的字符串，python返回一个新的对象，结果包含了以这对偏移标识的连续的内容，左边的开始是包含了下边界，但不包含上边界。

加号（+）是字符串连接运算符，星号（*）是重复操作。如下实例：

实例(Python 2.0+)
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
str = 'Hello World!'
print str # 输出完整字符串
print str[0] # 输出字符串中的第一个字符
print str[2:5] # 输出字符串中第三个至第五个之间的字符串
print str[2:] # 输出从第三个字符开始的字符串
print str * 2 # 输出字符串两次
print str + "TEST" # 输出连接的字符串
```
#### Python列表
List（列表） 是 Python 中使用最频繁的数据类型。

列表可以完成大多数集合类的数据结构实现。它支持字符，数字，字符串甚至可以包含列表（即嵌套）。

列表用 `[ ]` 标识，是 python 最通用的复合数据类型。

列表中值的切割也可以用到变量 `[头下标:尾下标]` ，就可以截取相应的列表，从左到右索引默认 0 开始，从右到左索引默认 -1 开始，下标可以为空表示取到头或尾。

加号 + 是列表连接运算符，星号 * 是重复操作。如下实例：

实例(Python 2.0+)
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
list = [ 'runoob', 786 , 2.23, 'john', 70.2 ]
tinylist = [123, 'john']
print list # 输出完整列表
print list[0] # 输出列表的第一个元素
print list[1:3] # 输出第二个至第三个元素 
print list[2:] # 输出从第三个开始至列表末尾的所有元素
print tinylist * 2 # 输出列表两次
print list + tinylist # 打印组合的列表
```
#### Python元组
元组是另一个数据类型，类似于List（列表）。

元组用"`()`"标识。内部元素用逗号隔开。但是元组不能二次赋值，相当于只读列表。

实例(Python 2.0+)
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
tuple = ( 'runoob', 786 , 2.23, 'john', 70.2 )
tinytuple = (123, 'john')
print tuple # 输出完整元组
print tuple[0] # 输出元组的第一个元素
print tuple[1:3] # 输出第二个至第三个的元素 
print tuple[2:] # 输出从第三个开始至列表末尾的所有元素
print tinytuple * 2 # 输出元组两次
print tuple + tinytuple # 打印组合的元组
```
#### Python 字典
字典(dictionary)是除列表以外python之中最灵活的内置数据结构类型。列表是有序的对象集合，字典是无序的对象集合。

两者之间的区别在于：字典当中的元素是通过键来存取的，而不是通过偏移存取。

字典用"`{ }`"标识。字典由索引(key)和它对应的值value组成。

实例(Python 2.0+)
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
dict = {}
dict['one'] = "This is one"
dict[2] = "This is two"
tinydict = {'name': 'john','code':6734, 'dept': 'sales'}
print dict['one'] # 输出键为'one' 的值
print dict[2] # 输出键为 2 的值
print tinydict # 输出完整的字典
print tinydict.keys() # 输出所有键
print tinydict.values() # 输出所有值
```
#### Python数据类型转换
有时候，我们需要对数据内置的类型进行转换，数据类型的转换，你只需要将数据类型作为函数名即可。

以下几个内置的函数可以执行数据类型之间的转换。这些函数返回一个新的对象，表示转换的值。
<table>
<tbody><tr><th>函数</th><th>描述</th></tr>
<tr valign="top">
<td>
<p>int(x [,base])</p>
</td>
<td>
<p>将x转换为一个整数</p>
</td>
</tr>
<tr valign="top">
<td>
<p>long(x [,base] )</p>
</td>
<td>
<p>将x转换为一个长整数</p>
</td>
</tr>
<tr valign="top">
<td>
<p>float(x)</p>
</td>
<td>
<p>将x转换到一个浮点数</p>
</td>
</tr>
<tr valign="top">
<td>
<p>complex(real [,imag])</p>
</td>
<td>
<p>创建一个复数</p>
</td>
</tr>
<tr valign="top">
<td>
<p>str(x)</p>
</td>
<td>
<p>将对象 x 转换为字符串</p>
</td>
</tr>
<tr valign="top">
<td>
<p>repr(x)</p>
</td>
<td>
<p>将对象 x 转换为表达式字符串</p>
</td>
</tr>
<tr valign="top">
<td>
<p>eval(str)</p>
</td>
<td>
<p>用来计算在字符串中的有效Python表达式,并返回一个对象</p>
</td>
</tr>
<tr valign="top">
<td>
<p>tuple(s)</p>
</td>
<td>
<p>将序列 s 转换为一个元组</p>
</td>
</tr>
<tr valign="top">
<td>
<p>list(s)</p>
</td>
<td>
<p>将序列 s 转换为一个列表</p>
</td>
</tr>
<tr valign="top">
<td>
<p>set(s)</p>
</td>
<td>
<p>转换为可变集合</p>
</td>
</tr>
<tr valign="top">
<td>
<p>dict(d)</p>
</td>
<td>
<p>创建一个字典。d 必须是一个序列 (key,value)元组。</p>
</td>
</tr>
<tr valign="top">
<td>
<p>frozenset(s)</p>
</td>
<td>
<p>转换为不可变集合</p>
</td>
</tr>
<tr valign="top">
<td>
<p>chr(x)</p>
</td>
<td>
<p> 将一个整数转换为一个字符</p>
</td>
</tr>
<tr valign="top">
<td>
<p>unichr(x)</p>
</td>
<td>
<p>将一个整数转换为Unicode字符</p>
</td>
</tr>
<tr valign="top">
<td>
<p>ord(x)</p>
</td>
<td>
<p> 将一个字符转换为它的整数值</p>
</td>
</tr>
<tr valign="top">
<td>
<p>hex(x)</p>
</td>
<td>
<p> 将一个整数转换为一个十六进制字符串</p>
</td>
</tr>
<tr valign="top">
<td>
<p>oct(x)</p>
</td>
<td>
<p> 将一个整数转换为一个八进制字符串</p>
</td>
</tr>
</tbody></table>

### Python 运算符
Python语言支持以下类型的运算符:
* 算术运算符
* 比较（关系）运算符
* 赋值运算符
* 逻辑运算符
* 位运算符
* 成员运算符
* 身份运算符
* 运算符优先级
#### Python算术运算符
以下假设变量： a=10，b=20：
<table class="reference">
<tbody><tr>
<th>运算符</th><th>描述</th><th>实例</th>
</tr>
<tr>
<td>+</td><td>加 - 两个对象相加</td><td> a + b 输出结果 30</td>
</tr>
<tr>
<td>-</td><td>减 - 得到负数或是一个数减去另一个数</td><td> a - b 输出结果 -10</td>
</tr>
<tr>
<td>*</td><td>乘 - 两个数相乘或是返回一个被重复若干次的字符串</td><td> a * b 输出结果 200</td>
</tr>
<tr>
<td>/</td><td>除 - x除以y</td><td> b / a 输出结果 2</td>
</tr>
<tr>
<td>%</td><td>取模 - 返回除法的余数</td><td> b % a 输出结果 0</td>
</tr>
<tr>
<td>**</td><td>幂 - 返回x的y次幂</td><td> a**b 为10的20次方， 输出结果 100000000000000000000</td>
</tr>
<tr>
<td>//</td><td>取整除 - 返回商的整数部分</td><td> 9//2 输出结果 4 , 9.0//2.0 输出结果 4.0</td>
</tr>
</tbody></table>
注意：Python2.x 里，整数除整数，只能得出整数。如果要得到小数部分，把其中一个数改成浮点数即可。

```py
>>> 1/2
0
>>> 1.0/2
0.5
>>> 1/float(2)
0.5
```
#### Python比较运算符
以下假设变量a为10，变量b为20：
<table class="reference">
<tbody><tr>
<th width="10%">运算符</th><th>描述</th><th>实例</th>
</tr>
<tr>
<td>==</td><td> 等于 - 比较对象是否相等</td><td> (a == b) 返回 False。 </td>
</tr>
<tr>
<td>!=</td><td> 不等于 - 比较两个对象是否不相等</td><td> (a != b) 返回 true. </td>
</tr>
<tr>
<td>&lt;&gt;</td><td>不等于 - 比较两个对象是否不相等</td><td> (a &lt;&gt; b) 返回 true。这个运算符类似 != 。</td>
</tr>
<tr>
<td>&gt;</td><td> 大于 - 返回x是否大于y</td><td> (a &gt; b) 返回 False。</td>
</tr>
<tr>
<td>&lt;</td><td> 小于 - 返回x是否小于y。所有比较运算符返回1表示真，返回0表示假。这分别与特殊的变量True和False等价。</td><td> (a &lt; b) 返回 true。 </td>
</tr>
<tr>
<td>&gt;=</td><td> 大于等于 - 返回x是否大于等于y。</td><td> (a &gt;= b) 返回 False。</td>
</tr>
<tr>
<td>&lt;=</td><td> 小于等于 - 返回x是否小于等于y。</td><td> (a &lt;= b) 返回 true。 </td>
</tr>
</tbody></table>

#### Python赋值运算符
以下假设变量a为10，变量b为20：
<table class="reference">
<tbody><tr>
<th>运算符</th><th>描述</th><th>实例</th>
</tr>
<tr>
<td>=</td><td>简单的赋值运算符</td><td> c = a + b 将 a + b 的运算结果赋值为 c</td>
</tr>
<tr>
<td>+=</td><td>加法赋值运算符</td><td> c += a 等效于 c = c + a</td>
</tr>
<tr>
<td>-=</td><td>减法赋值运算符</td><td> c -= a 等效于 c = c - a</td>
</tr>
<tr>
<td>*=</td><td>乘法赋值运算符</td><td> c *= a 等效于 c = c * a</td>
</tr>
<tr>
<td>/=</td><td>除法赋值运算符</td><td> c /= a 等效于 c = c / a</td>
</tr>
<tr>
<td>%=</td><td>取模赋值运算符</td><td> c %= a 等效于 c = c % a</td>
</tr>
<tr>
<td>**=</td><td>幂赋值运算符</td><td> c **= a 等效于 c = c ** a</td>
</tr>
<tr>
<td>//=</td><td> 取整除赋值运算符</td><td> c //= a 等效于 c = c // a</td>
</tr>
</tbody></table>

#### Python位运算符
按位运算符是把数字看作二进制来进行计算的。Python中的按位运算法则如下：

下表中变量 a 为 60，b 为 13，二进制格式如下：
```py
a = 0011 1100

b = 0000 1101

-----------------

a&b = 0000 1100

a|b = 0011 1101

a^b = 0011 0001

~a  = 1100 0011
```
<table class="reference">
<tbody><tr>
<th>运算符</th><th>描述</th><th>实例</th>
</tr>
<tr>
<td>&amp;</td><td>按位与运算符：参与运算的两个值,如果两个相应位都为1,则该位的结果为1,否则为0</td><td> (a &amp; b) 输出结果 12 ，二进制解释： 0000 1100</td>
</tr>
<tr>
<td>|</td><td> 按位或运算符：只要对应的二个二进位有一个为1时，结果位就为1。</td><td> (a | b) 输出结果 61 ，二进制解释： 0011 1101</td>
</tr>
<tr>
<td>^</td><td>按位异或运算符：当两对应的二进位相异时，结果为1 </td><td> (a ^ b) 输出结果 49 ，二进制解释： 0011 0001</td>
</tr>
<tr>
<td>~</td><td> 按位取反运算符：对数据的每个二进制位取反,即把1变为0,把0变为1 。<span class="marked">~x</span> 类似于 <span class="marked">-x-1</span></td><td> (~a ) 输出结果 -61 ，二进制解释： 1100 0011，在一个有符号二进制数的补码形式。</td>
</tr>
<tr>
<td>&lt;&lt;</td><td>左移动运算符：运算数的各二进位全部左移若干位，由 <span class="marked">&lt;&lt;</span> 右边的数字指定了移动的位数，高位丢弃，低位补0。</td><td> a &lt;&lt; 2 输出结果 240 ，二进制解释： 1111 0000</td>
</tr>
<tr>
<td>&gt;&gt;</td><td>右移动运算符：把"&gt;&gt;"左边的运算数的各二进位全部右移若干位，<span class="marked">&gt;&gt;</span> 右边的数字指定了移动的位数 </td><td> a &gt;&gt; 2 输出结果 15 ，二进制解释： 0000 1111</td>
</tr>
</tbody></table>

#### Python逻辑运算符
Python语言支持逻辑运算符，以下假设变量 a 为 10, b为 20:
<table class="reference">
<tbody><tr>
<th>运算符</th><th>逻辑表达式</th><th>描述</th><th>实例</th>
</tr>
<tr>
<td>and</td><td>x and y</td><td> 布尔"与" - 如果 x 为 False，x and y 返回 False，否则它返回 y 的计算值。 </td><td> (a and b) 返回 20。</td>
</tr>
<tr>
<td>or</td><td>x or y</td><td>布尔"或" - 如果 x 是非 0，它返回 x 的值，否则它返回 y 的计算值。</td><td> (a or b) 返回 10。</td>
</tr>
<tr><td>not</td><td>not x</td><td>布尔"非" - 如果 x 为 True，返回 False 。如果 x 为 False，它返回 True。</td><td> not(a and b) 返回 False </td>
</tr>
</tbody></table>

#### Python成员运算符
除了以上的一些运算符之外，Python还支持成员运算符，测试实例中包含了一系列的成员，包括字符串，列表或元组。
<table class="reference">
<tbody><tr>
<th>运算符</th><th>描述</th><th>实例</th>
</tr>
<tr>
<td>in</td><td>
如果在指定的序列中找到值返回 True，否则返回 False。</td>
<td> x 在 y 序列中 , 如果 x 在 y 序列中返回 True。</td>
</tr>
<tr>
<td>not in</td><td>如果在指定的序列中没有找到值返回 True，否则返回 False。</td>
<td>x 不在 y 序列中 , 如果 x 不在 y 序列中返回 True。</td>
</tr>
</tbody></table>

以下实例演示了Python所有成员运算符的操作：

实例(Python 2.0+)
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
a = 10
b = 20
list = [1, 2, 3, 4, 5 ];
if ( a in list ):
print "1 - 变量 a 在给定的列表中 list 中"
else:
print "1 - 变量 a 不在给定的列表中 list 中"
if ( b not in list ):
print "2 - 变量 b 不在给定的列表中 list 中"
else:
print "2 - 变量 b 在给定的列表中 list 中"
# 修改变量 a 的值
a = 2
if ( a in list ):
print "3 - 变量 a 在给定的列表中 list 中"
else:
print "3 - 变量 a 不在给定的列表中 list 中"
```
#### Python身份运算符
身份运算符用于比较两个对象的存储单元
<table class="reference">
<tbody><tr>
<th width="10%">运算符</th><th>描述</th><th>实例</th>
</tr>
<tr>
<td>is</td><td>
is 是判断两个标识符是不是引用自一个对象</td><td> <strong>x is y</strong>, 类似 <strong>id(x) == id(y)</strong> , 如果引用的是同一个对象则返回 True，否则返回 False</td>
</tr>
<tr>
<td>is not</td><td>is not 是判断两个标识符是不是引用自不同对象</td><td><strong> x is not y</strong> ， 类似 <strong>id(a) != id(b)</strong>。如果引用的不是同一个对象则返回结果 True，否则返回 False。 </td>
</tr>
</tbody></table>

注： id() 函数用于获取对象内存地址。

以下实例演示了Python所有身份运算符的操作：

实例(Python 2.0+)
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
a = 20
b = 20
if ( a is b ):
print "1 - a 和 b 有相同的标识"
else:
print "1 - a 和 b 没有相同的标识"
if ( a is not b ):
print "2 - a 和 b 没有相同的标识"
else:
print "2 - a 和 b 有相同的标识"
# 修改变量 b 的值
b = 30
if ( a is b ):
print "3 - a 和 b 有相同的标识"
else:
print "3 - a 和 b 没有相同的标识"
if ( a is not b ):
print "4 - a 和 b 没有相同的标识"
else:
print "4 - a 和 b 有相同的标识"
```
以上实例输出结果：
```
1 - a 和 b 有相同的标识
2 - a 和 b 有相同的标识
3 - a 和 b 没有相同的标识
4 - a 和 b 没有相同的标识
```
is 与 == 区别：

is 用于判断两个变量引用对象是否为同一个， == 用于判断引用变量的值是否相等。
```py
>>> a = [1, 2, 3]
>>> b = a
>>> b is a 
True
>>> b == a
True
>>> b = a[:]
>>> b is a
False
>>> b == a
True
```
#### Python运算符优先级
以下表格列出了从最高到最低优先级的所有运算符：
<table class="reference">
<tbody><tr><th>运算符</th><th>描述</th></tr>
<tr>
<td>**</td>
<td>指数 (最高优先级)</td>
</tr><tr>
<td>~ + -</td>
<td>按位翻转, 一元加号和减号 (最后两个的方法名为 +@ 和 -@)</td>
</tr><tr>
<td>* / % //</td>
<td>乘，除，取模和取整除</td>
</tr><tr>
<td>+ -</td>
<td>加法减法</td>
</tr><tr>
<td>&gt;&gt; &lt;&lt;</td>
<td>右移，左移运算符</td>
</tr><tr>
<td>&amp;</td>
<td>位 'AND'</td>
</tr><tr>
<td>^ |</td>
<td>位运算符</td>
</tr><tr>
<td>&lt;= &lt; &gt; &gt;=</td>
<td>比较运算符</td>
</tr><tr>
<td>&lt;&gt; == !=</td>
<td>等于运算符</td>
</tr>
<tr>
<td>= %= /= //= -= += *= **=</td>
<td>赋值运算符</td>
</tr>
<tr>
<td>is is not</td>
<td>身份运算符</td>
</tr>
<tr>
<td>in not in</td>
<td>成员运算符</td>
</tr><tr>
<td>not or and</td>
<td>逻辑运算符</td>
</tr>
</tbody></table>

### Python 条件语句
Python条件语句是通过一条或多条语句的执行结果（True或者False）来决定执行的代码块。

Python程序语言指定任何非0和非空（null）值为true，0 或者 null为false。

Python 编程中 if 语句用于控制程序的执行，基本形式为：
```py
if 判断条件：
    执行语句……
else：
    执行语句……
```
其中"判断条件"成立时（非零），则执行后面的语句，而执行内容可以多行，以缩进来区分表示同一范围。

else 为可选语句，当需要在条件不成立时执行内容则可以执行相关语句，具体例子如下：

```py
实例
#!/usr/bin/python
# -*- coding: UTF-8 -*-
# 例1：if 基本用法
flag = False
name = 'luren'
if name == 'python': # 判断变量否为'python'
flag = True # 条件成立时设置标志为真
print 'welcome boss' # 并输出欢迎信息
else:
print name # 条件不成立时输出变量名称
```
输出结果为：
```
luren            # 输出结果
```
if 语句的判断条件可以用>（大于）、<(小于)、==（等于）、>=（大于等于）、<=（小于等于）来表示其关系。

当判断条件为多个值时，可以使用以下形式：
```py
if 判断条件1:
    执行语句1……
elif 判断条件2:
    执行语句2……
elif 判断条件3:
    执行语句3……
else:
    执行语句4……
```
实例如下：
```py
实例
#!/usr/bin/python
# -*- coding: UTF-8 -*-
# 例2：elif用法
num = 5
if num == 3: # 判断num的值
print 'boss'
elif num == 2:
print 'user'
elif num == 1:
print 'worker'
elif num < 0: # 值小于零时输出
print 'error'
else:
print 'roadman' # 条件均不成立时输出
```
输出结果为：
```py
roadman        # 输出结果
```
由于 python 并不支持 switch 语句，所以多个条件判断，只能用 elif 来实现，如果判断需要多个条件需同时判断时，可以使用 or （或），表示两个条件有一个成立时判断条件成功；使用 and （与）时，表示只有两个条件同时成立的情况下，判断条件才成功。
```py
实例
#!/usr/bin/python
# -*- coding: UTF-8 -*-
# 例3：if语句多个条件
num = 9
if num >= 0 and num <= 10: # 判断值是否在0~10之间
print 'hello'
# 输出结果: hello
num = 10
if num < 0 or num > 10: # 判断值是否在小于0或大于10
print 'hello'
else:
print 'undefine'
# 输出结果: undefine
num = 8
# 判断值是否在0~5或者10~15之间
if (num >= 0 and num <= 5) or (num >= 10 and num <= 15):
print 'hello'
else:
print 'undefine'
# 输出结果: undefine
```
当if有多个条件时可使用括号来区分判断的先后顺序，括号中的判断优先执行，此外 and 和 or 的优先级低于>（大于）、<（小于）等判断符号，即大于和小于在没有括号的情况下会比与或要优先判断。
#### 简单的语句组
你也可以在同一行的位置上使用if条件判断语句，如下实例：
```py
实例
#!/usr/bin/python 
# -*- coding: UTF-8 -*-
var = 100
if ( var == 100 ) : print "变量 var 的值为100"
print "Good bye!"
```
以上代码执行输出结果如下：
```py
变量 var 的值为100
Good bye!
```
### Python 循环语句
循环语句允许我们执行一个语句或语句组多次。

Python提供了for循环和while循环（在Python中没有do..while循环）:
<table class="reference">
<tbody><tr><th style="width:30%">循环类型</th><th>描述</th></tr>
<tr><td>while 循环</td><td>在给定的判断条件为 true 时执行循环体，否则退出循环体。</td></tr>
<tr><td>for 循环</td><td>重复执行语句</td></tr>
<tr><td>嵌套循环</td><td>你可以在while循环体中嵌套for循环</td></tr>
</tbody></table>

#### 循环控制语句
循环控制语句可以更改语句执行的顺序。Python支持以下循环控制语句：
<table class="reference">
<tbody><tr><th style="width:30%">控制语句</th><th>描述</th></tr>
<tr><td>break 语句</td><td>在语句块执行过程中终止循环，并且跳出整个循环</td></tr>
<tr><td>continue 语句</td><td>在语句块执行过程中终止当前循环，跳出该次循环，执行下一次循环。</td></tr>
<tr><td>pass 语句</td><td>pass是空语句，是为了保持程序结构的完整性。</td></tr>
</tbody></table>

### Python While 循环语句
Python 编程中 while 语句用于循环执行程序，即在某条件下，循环执行某段程序，以处理需要重复处理的相同任务。其基本形式为：
```py
while 判断条件：
    执行语句……
```
执行语句可以是单个语句或语句块。判断条件可以是任何表达式，任何非零、或非空（null）的值均为true。

当判断条件假false时，循环结束。

while 语句时还有另外两个重要的命令 continue，break 来跳过循环，continue 用于跳过该次循环，break 则是用于退出循环，此外"判断条件"还可以是个常值，表示循环必定成立，具体用法如下：
```py
# continue 和 break 用法
i = 1
while i < 10:
i += 1
if i%2 > 0: # 非双数时跳过输出
continue
print i # 输出双数2、4、6、8、10
i = 1
while 1: # 循环条件为1必定成立
print i # 输出1~10
i += 1
if i > 10: # 当i大于10时跳出循环
break
```
#### 无限循环
如果条件判断语句永远为 true，循环将会无限的执行下去，如下实例：
```py
实例
#!/usr/bin/python
# -*- coding: UTF-8 -*-
var = 1
while var == 1 : # 该条件永远为true，循环将无限执行下去
num = raw_input("Enter a number :")
print "You entered: ", num
print "Good bye!"
```
 **注意：以上的无限循环你可以使用 CTRL+C 来中断循环。** 
#### 循环使用 else 语句
在 python 中，while … else 在循环条件为 false 时执行 else 语句块：
```py
实例
#!/usr/bin/python
count = 0
while count < 5:
print count, " is less than 5"
count = count + 1
else:
print count, " is not less than 5"
```
以上实例输出结果为：
```py
0 is less than 5
1 is less than 5
2 is less than 5
3 is less than 5
4 is less than 5
5 is not less than 5
```
#### 简单语句组
类似 if 语句的语法，如果你的 while 循环体中只有一条语句，你可以将该语句与while写在同一行中， 如下所示：
```py
实例
#!/usr/bin/python
flag = 1
while (flag): print 'Given flag is really true!'
print "Good bye!"
```
注意：以上的无限循环你可以使用 CTRL+C 来中断循环。

### Python for 循环语句
Python for循环可以遍历任何序列的项目，如一个列表或者一个字符串。

语法：

for循环的语法格式如下：
```py
for iterating_var in sequence:
   statements(s)
```
```py
实例
#!/usr/bin/python
# -*- coding: UTF-8 -*-
for letter in 'Python': # 第一个实例
print '当前字母 :', letter
fruits = ['banana', 'apple', 'mango']
for fruit in fruits: # 第二个实例
print '当前水果 :', fruit
print "Good bye!"
```
#### 通过序列索引迭代
另外一种执行循环的遍历方式是通过索引，如下实例：
```py
实例
#!/usr/bin/python
# -*- coding: UTF-8 -*-
fruits = ['banana', 'apple', 'mango']
for index in range(len(fruits)):
print '当前水果 :', fruits[index]
print "Good bye!"
```
以上实例我们使用了内置函数 len() 和 range(),函数 len() 返回列表的长度，即元素的个数。 range返回一个序列的数。
#### 循环使用 else 语句
在 python 中，for … else 表示这样的意思，for 中的语句和普通的没有区别，else 中的语句会在循环正常执行完（即 for 不是通过 break 跳出而中断的）的情况下执行，while … else 也是一样。
```py
实例
#!/usr/bin/python
# -*- coding: UTF-8 -*-
for num in range(10,20): # 迭代 10 到 20 之间的数字
for i in range(2,num): # 根据因子迭代
if num%i == 0: # 确定第一个因子
j=num/i # 计算第二个因子
print '%d 等于 %d * %d' % (num,i,j)
break # 跳出当前循环
else: # 循环的 else 部分
print num, '是一个质数'
```
### Python 循环嵌套
Python 语言允许在一个循环体里面嵌入另一个循环。

你可以在循环体内嵌入其他的循环体，如在while循环中可以嵌入for循环， 反之，你可以在for循环中嵌入while循环。
```py
实例：
以下实例使用了嵌套循环输出2~100之间的素数：
实例
#!/usr/bin/python
# -*- coding: UTF-8 -*-
i = 2
while(i < 100):
j = 2
while(j <= (i/j)):
if not(i%j): break
j = j + 1
if (j > i/j) : print i, " 是素数"
i = i + 1
print "Good bye!"
```
### Python break 语句
Python break语句，就像在C语言中，打破了最小封闭for或while循环。

break语句用来终止循环语句，即循环条件没有False条件或者序列还没被完全递归完，也会停止执行循环语句。

break语句用在while和for循环中。

如果您使用嵌套循环，break语句将停止执行最深层的循环，并开始执行下一行代码。

Python语言 break 语句语法：
```py
break
```
### Python continue 语句
Python continue 语句跳出本次循环，而break跳出整个循环。

continue 语句用来告诉Python跳过当前循环的剩余语句，然后继续进行下一轮循环。

continue语句用在while和for循环中。

Python 语言 continue 语句语法格式如下：
```py
continue
```
### Python pass 语句
Python pass是空语句，是为了保持程序结构的完整性。

pass 不做任何事情，一般用做占位语句。可以用来占位，今后再来填充要运行的语句。

Python 语言 pass 语句语法格式如下：
```py
pass
```
实例：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*- 

# 输出 Python 的每个字母
for letter in 'Python':
   if letter == 'h':
      pass
      print '这是 pass 块'
   print '当前字母 :', letter

print "Good bye!"
```
### Python Number(数字)
数字的基础情况前面讲过了这里就不赘述了。
#### Python math 模块、cmath 模块

Python 中数学运算常用的函数基本都在 math 模块、cmath 模块中。

Python math 模块提供了许多对浮点数的数学运算函数。

Python cmath 模块包含了一些用于复数运算的函数。

cmath 模块的函数跟 math 模块函数基本一致，区别是 cmath 模块运算的是复数，math 模块运算的是数学运算。

要使用 math 或 cmath 函数必须先导入：
```py
import math
```
查看 math 查看包中的内容:
```py
>>> import math
>>> dir(math)
['__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'acos', 'acosh', 'asin', 'asinh', 'atan', 'atan2', 'atanh', 'ceil', 'copysign', 'cos', 'cosh', 'degrees', 'e', 'erf', 'erfc', 'exp', 'expm1', 'fabs', 'factorial', 'floor', 'fmod', 'frexp', 'fsum', 'gamma', 'gcd', 'hypot', 'inf', 'isclose', 'isfinite', 'isinf', 'isnan', 'ldexp', 'lgamma', 'log', 'log10', 'log1p', 'log2', 'modf', 'nan', 'pi', 'pow', 'radians', 'sin', 'sinh', 'sqrt', 'tan', 'tanh', 'tau', 'trunc']
>>>
```
查看 cmath 查看包中的内容
```py
>>> import cmath
>>> dir(cmath)
['__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'acos', 'acosh', 'asin', 'asinh', 'atan', 'atanh', 'cos', 'cosh', 'e', 'exp', 'inf', 'infj', 'isclose', 'isfinite', 'isinf', 'isnan', 'log', 'log10', 'nan', 'nanj', 'phase', 'pi', 'polar', 'rect', 'sin', 'sinh', 'sqrt', 'tan', 'tanh', 'tau']
>>>
```
#### Python数学函数
<table class="reference">
<tbody><tr>
<th>函数</th><th>返回值 ( 描述 )</th></tr>
<tr><td>abs(x)</td><td>返回数字的绝对值，如abs(-10) 返回 10</td></tr>
<tr><td>ceil(x) </td><td>返回数字的上入整数，如math.ceil(4.1) 返回 5</td></tr>
<tr><td>cmp(x, y)</td><td> 如果 x &lt; y 返回 -1, 如果 x == y 返回 0, 如果 x &gt; y 返回 1</td></tr>
<tr><td>exp(x) </td><td>返回e的x次幂(e<sup>x</sup>),如math.exp(1) 返回2.718281828459045</td></tr>
<tr><td>fabs(x)</td><td>返回数字的绝对值，如math.fabs(-10) 返回10.0</td></tr>
<tr><td>floor(x) </td><td>返回数字的下舍整数，如math.floor(4.9)返回 4</td></tr>
<tr><td>log(x) </td><td>如math.log(math.e)返回1.0,math.log(100,10)返回2.0</td></tr>
<tr><td>log10(x) </td><td>返回以10为基数的x的对数，如math.log10(100)返回 2.0</td></tr>
<tr><td>max(x1, x2,...) </td><td>返回给定参数的最大值，参数可以为序列。</td></tr>
<tr><td>min(x1, x2,...) </td><td>返回给定参数的最小值，参数可以为序列。</td></tr>
<tr><td>modf(x) </td><td>返回x的整数部分与小数部分，两部分的数值符号与x相同，整数部分以浮点型表示。</td></tr>
<tr><td>pow(x, y)</td><td> x**y 运算后的值。</td></tr>
<tr><td>round(x [,n])</td><td>返回浮点数x的四舍五入值，如给出n值，则代表舍入到小数点后的位数。</td></tr>
<tr><td>sqrt(x) </td><td>返回数字x的平方根</td></tr>
</tbody></table>

#### Python随机数函数
随机数可以用于数学，游戏，安全等领域中，还经常被嵌入到算法中，用以提高算法效率，并提高程序的安全性。

Python包含以下常用随机数函数：
<table class="reference">
<tbody><tr>
<th>函数</th><th>描述</th></tr>
<tr><td>choice(seq)</td><td>从序列的元素中随机挑选一个元素，比如random.choice(range(10))，从0到9中随机挑选一个整数。</td></tr>
<tr><td>randrange ([start,] stop [,step]) </td><td>从指定范围内，按指定基数递增的集合中获取一个随机数，基数缺省值为1</td></tr>
<tr><td>random() </td><td> 随机生成下一个实数，它在[0,1)范围内。</td></tr>
<tr><td>seed([x]) </td><td>改变随机数生成器的种子seed。如果你不了解其原理，你不必特别去设定seed，Python会帮你选择seed。</td></tr>
<tr><td>shuffle(lst) </td><td>将序列的所有元素随机排序</td></tr>
<tr><td>uniform(x, y)</td><td>随机生成下一个实数，它在[x,y]范围内。</td></tr>
</tbody></table>

#### Python三角函数
<table class="reference">
<tbody><tr>
<th>函数</th><th>描述</th></tr>
<tr><td>acos(x)</td><td>返回x的反余弦弧度值。</td></tr>
<tr><td>asin(x)</td><td>返回x的反正弦弧度值。</td></tr>
<tr><td>atan(x)</td><td>返回x的反正切弧度值。</td></tr>
<tr><td>atan2(y, x)</td><td>返回给定的 X 及 Y 坐标值的反正切值。</td></tr>
<tr><td>cos(x)</td><td>返回x的弧度的余弦值。</td></tr>
<tr><td>hypot(x, y)</td><td>返回欧几里德范数 sqrt(x*x + y*y)。 </td></tr>
<tr><td>sin(x)</td><td>返回的x弧度的正弦值。</td></tr>
<tr><td>tan(x)</td><td>返回x弧度的正切值。</td></tr>
<tr><td>degrees(x)</td><td>将弧度转换为角度,如degrees(math.pi/2) ， 返回90.0</td></tr>
<tr><td>radians(x)</td><td>将角度转换为弧度</td></tr>
</tbody></table>

#### Python数学常量
<table class="reference">
<tbody><tr>
<th>常量</th><th>描述</th></tr>
<tr><td>pi</td><td>数学常量 pi（圆周率，一般以π来表示）</td></tr>
<tr><td>e</td><td>数学常量 e，e即自然常数（自然常数）。</td></tr>
</tbody></table>

### Python 字符串
字符串是 Python 中最常用的数据类型。我们可以使用引号('或")来创建字符串。

创建字符串很简单，只要为变量分配一个值即可。例如：
```py
var1 = 'Hello World!'
var2 = "Python Runoob"
```
#### Python转义字符
在需要在字符中使用特殊字符时，python用反斜杠(\\)转义字符。如下表：
<table class="reference">
<thead>
<tr>
<th>转义字符</th><th>描述</th>
</tr>
</thead>
<tbody>
<tr>
<td>\(在行尾时)</td>
<td>续行符</td>
</tr>
<tr>
<td>\\</td>
<td>反斜杠符号</td>
</tr>
<tr>
<td>\'</td>
<td>单引号</td>
</tr>
<tr>
<td>\"</td>
<td>双引号</td>
</tr>
<tr>
<td>\a</td>
<td>响铃</td>
</tr>
<tr>
<td>\b</td>
<td>退格(Backspace)</td>
</tr>
<tr>
<td>\e</td>
<td>转义</td>
</tr>
<tr>
<td>\000</td>
<td>空</td>
</tr>
<tr>
<td>\n</td>
<td>换行</td>
</tr>
<tr>
<td>\v</td>
<td>纵向制表符</td>
</tr>
<tr>
<td>\t</td>
<td>横向制表符</td>
</tr>
<tr>
<td>\r</td>
<td>回车</td>
</tr>
<tr>
<td>\f</td>
<td>换页</td>
</tr>
<tr>
<td>\oyy</td>
<td>八进制数，yy代表的字符，例如：\o12代表换行</td>
</tr>
<tr>
<td>\xyy</td>
<td>十六进制数，yy代表的字符，例如：\x0a代表换行</td>
</tr>
<tr>
<td>\other</td>
<td>其它的字符以普通格式输出</td>
</tr>
</tbody>
</table>

#### Python字符串运算符
下表实例变量 a 值为字符串 "Hello"，b 变量值为 "Python"：
<table class="reference">
<tbody><tr>
<th width="10%">操作符</th><th width="60%">描述</th><th>实例</th>
</tr>
<tr>
<td>+</td><td>字符串连接</td><td>
<div class="hl-main"><span class="hl-code">&gt;&gt;&gt;</span><span class="hl-identifier">a</span><span class="hl-code"> + </span><span class="hl-identifier">b</span><span class="hl-code">
</span><span class="hl-quotes">'</span><span class="hl-string">HelloPython</span><span class="hl-quotes">'</span></div>
</td>
</tr>
<tr>
<td>*</td><td>重复输出字符串</td><td>
<div class="hl-main"><span class="hl-code">&gt;&gt;&gt;</span><span class="hl-identifier">a</span><span class="hl-code"> * </span><span class="hl-number">2</span><span class="hl-code">
</span><span class="hl-quotes">'</span><span class="hl-string">HelloHello</span><span class="hl-quotes">'</span></div>
</td>
</tr>
<tr>
<td>[]</td><td>通过索引获取字符串中字符</td><td>
<div class="hl-main"><span class="hl-code">&gt;&gt;&gt;</span><span class="hl-identifier">a</span><span class="hl-brackets">[</span><span class="hl-number">1</span><span class="hl-brackets">]</span><span class="hl-code">
</span><span class="hl-quotes">'</span><span class="hl-string">e</span><span class="hl-quotes">'</span></div>
</td>
</tr>
<tr>
<td>[ : ]</td><td>截取字符串中的一部分</td><td>
<div class="hl-main"><span class="hl-code">&gt;&gt;&gt;</span><span class="hl-identifier">a</span><span class="hl-brackets">[</span><span class="hl-number">1</span><span class="hl-code">:</span><span class="hl-number">4</span><span class="hl-brackets">]</span><span class="hl-code">
</span><span class="hl-quotes">'</span><span class="hl-string">ell</span><span class="hl-quotes">'</span></div>
</td>
</tr>
<tr>
<td>in</td><td>成员运算符 - 如果字符串中包含给定的字符返回 True </td><td>
<div class="hl-main"><span class="hl-code">&gt;&gt;&gt;</span><span class="hl-quotes">&quot;</span><span class="hl-string">H</span><span class="hl-quotes">&quot;</span><span class="hl-code"> </span><span class="hl-reserved">in</span><span class="hl-code"> </span><span class="hl-identifier">a</span><span class="hl-code">
</span><span class="hl-reserved">True</span></div>
</td>
</tr>
<tr>
<td>not in </td><td>成员运算符 - 如果字符串中不包含给定的字符返回 True </td><td>
<div class="hl-main"><span class="hl-code">&gt;&gt;&gt;</span><span class="hl-quotes">&quot;</span><span class="hl-string">M</span><span class="hl-quotes">&quot;</span><span class="hl-code"> </span><span class="hl-reserved">not</span><span class="hl-code"> </span><span class="hl-reserved">in</span><span class="hl-code"> </span><span class="hl-identifier">a</span><span class="hl-code">
</span><span class="hl-reserved">True</span></div>
</td>
</tr>
<tr>
<td>r/R</td><td>原始字符串 - 原始字符串：所有的字符串都是直接按照字面的意思来使用，没有转义特殊或不能打印的字符。
原始字符串除在字符串的第一个引号前加上字母"r"（可以大小写）以外，与普通字符串有着几乎完全相同的语法。</td><td> <div class="hl-main"><span class="hl-code">&gt;&gt;&gt;</span><span class="hl-reserved">print</span><span class="hl-code"> </span><span class="hl-identifier">r</span><span class="hl-quotes">'</span><span class="hl-special">\n</span><span class="hl-quotes">'</span><span class="hl-code">
\</span><span class="hl-identifier">n</span><span class="hl-code">
&gt;&gt;&gt; </span><span class="hl-reserved">print</span><span class="hl-code"> </span><span class="hl-identifier">R</span><span class="hl-quotes">'</span><span class="hl-special">\n</span><span class="hl-quotes">'</span><span class="hl-code">
\</span><span class="hl-identifier">n</span></div> </td>
</tr>
<tr>
<td>%</td><td>格式字符串</td><td>请看下一章节</td>
</tr>
</tbody></table>

#### Python 字符串格式化
Python 支持格式化字符串的输出 。尽管这样可能会用到非常复杂的表达式，但最基本的用法是将一个值插入到一个有字符串格式符 %s 的字符串中。

在 Python 中，字符串格式化使用与 C 中 sprintf 函数一样的语法。

如下实例：
```py
#!/usr/bin/python

print "My name is %s and weight is %d kg!" % ('Zara', 21) 
```
python字符串格式化符号:
<table class="reference">
<tbody><tr><th>
 符 号</th>
<th>描述</th></tr><tr><td> %c</td><td>格式化字符及其ASCII码</td></tr><tr><td> %s</td><td>格式化字符串</td></tr><tr><td> %d</td><td>格式化整数</td></tr><tr><td> %u</td><td>格式化无符号整型</td></tr><tr><td> %o</td><td>格式化无符号八进制数</td></tr><tr><td> %x</td><td>格式化无符号十六进制数</td></tr><tr><td> %X</td><td>格式化无符号十六进制数（大写）</td></tr><tr><td> %f</td><td>格式化浮点数字，可指定小数点后的精度</td></tr><tr><td> %e</td><td>用科学计数法格式化浮点数</td></tr><tr><td> %E</td><td>作用同%e，用科学计数法格式化浮点数</td></tr><tr><td> %g</td><td>%f和%e的简写</td></tr><tr><td> %G</td><td>%f 和 %E 的简写</td></tr><tr><td> %p</td><td>用十六进制数格式化变量的地址</td></tr></tbody></table>

格式化操作符辅助指令:
<table class="reference">
<tbody><tr>
<th>符号</th><th>功能</th>
</tr>
<tr><td>*</td><td>定义宽度或者小数点精度 </td></tr>
<tr><td>-</td><td>用做左对齐 </td></tr>
<tr><td>+</td><td>在正数前面显示加号( + ) </td></tr>
<tr><td>&lt;sp&gt;</td><td>在正数前面显示空格 </td></tr>
<tr><td>#</td><td> 在八进制数前面显示零('0')，在十六进制前面显示'0x'或者'0X'(取决于用的是'x'还是'X')</td></tr>
<tr><td>0</td><td> 显示的数字前面填充'0'而不是默认的空格 </td></tr>
<tr><td>%</td><td> '%%'输出一个单一的'%' </td></tr>
<tr><td>(var)</td><td>映射变量(字典参数) </td></tr>
<tr><td>m.n.</td><td> m 是显示的最小总宽度,n 是小数点后的位数(如果可用的话)</td></tr>
</tbody></table>

Python2.6 开始，新增了一种格式化字符串的函数 str.format()，它增强了字符串格式化的功能。

#### Python三引号（triple quotes）
python中三引号可以将复杂的字符串进行复制:

python三引号允许一个字符串跨多行，字符串中可以包含换行符、制表符以及其他特殊字符。

三引号的语法是一对连续的单引号或者双引号（通常都是成对的用）。
```py
 >>> hi = '''hi 
there'''
>>> hi   # repr()
'hi\nthere'
>>> print hi  # str()
hi 
there  
```
三引号让程序员从引号和特殊字符串的泥潭里面解脱出来，自始至终保持一小块字符串的格式是所谓的WYSIWYG（所见即所得）格式的。

一个典型的用例是，当你需要一块HTML或者SQL时，这时用字符串组合，特殊字符串转义将会非常的繁琐。
```py
 errHTML = '''
<HTML><HEAD><TITLE>
Friends CGI Demo</TITLE></HEAD>
<BODY><H3>ERROR</H3>
<B>%s</B><P>
<FORM><INPUT TYPE=button VALUE=Back
ONCLICK="window.history.back()"></FORM>
</BODY></HTML>
'''
cursor.execute('''
CREATE TABLE users (  
login VARCHAR(8), 
uid INTEGER,
prid INTEGER)
''')
```
#### Unicode 字符串
Python 中定义一个 Unicode 字符串和定义一个普通字符串一样简单：
```py
>>> u'Hello World !'
u'Hello World !'
```
#### python的字符串内建函数
字符串方法是从python1.6到2.0慢慢加进来的——它们也被加到了Jython中。

这些方法实现了string模块的大部分方法，如下表所示列出了目前字符串内建支持的方法，所有的方法都包含了对Unicode的支持，有一些甚至是专门用于Unicode的。
<table class="reference">
<tbody>
<tr>
<th>
<strong>方法</strong>
</th>
<th>
<strong>描述</strong>
</th>
</tr>
<tr>
<td>
<p>string.capitalize()</p>
</td>
<td>
<p>把字符串的第一个字符大写</p>
</td>
</tr>
<tr>
<td>
<p>string.center(width)</p>
</td>
<td>
<p>返回一个原字符串居中,并使用空格填充至长度 width 的新字符串</p>
</td>
</tr>
<tr>
<td>
<p><strong>string.count(str, beg=0, end=len(string))</strong></p>
</td>
<td>
<p>返回 str 在 string 里面出现的次数，如果 beg 或者 end 指定则返回指定范围内 str 出现的次数</p>
</td>
</tr>
<tr>
<td>
<p>string.decode(encoding='UTF-8', errors='strict')</p>
</td>
<td>
<p>以 encoding 指定的编码格式解码 string，如果出错默认报一个 ValueError 的 异 常 ， 除非 errors 指 定 的 是 'ignore' 或 者'replace'</p>
</td>
</tr>
<tr>
<td>
<p>string.encode(encoding='UTF-8', errors='strict')</p>
</td>
<td>
<p>以 encoding 指定的编码格式编码 string，如果出错默认报一个ValueError 的异常，除非 errors 指定的是'ignore'或者'replace'</p>
</td>
</tr>
<tr>
<td>
<p><strong>string.endswith(obj, beg=0, end=len(string))</strong></p>
</td>
<td>
<p>检查字符串是否以 obj 结束，如果beg 或者 end 指定则检查指定的范围内是否以 obj 结束，如果是，返回 True,否则返回 False.</p>
</td>
</tr>
<tr>
<td>
<p>string.expandtabs(tabsize=8)</p>
</td>
<td>
<p>把字符串 string 中的 tab 符号转为空格，tab 符号默认的空格数是 8。</p>
</td>
</tr>
<tr>
<td>
<p><strong>string.find(str, beg=0, end=len(string))</strong></p>
</td>
<td>
<p>检测 str 是否包含在 string 中，如果 beg 和 end 指定范围，则检查是否包含在指定范围内，如果是返回开始的索引值，否则返回-1</p>
</td>
</tr>
<tr>
<td>
<p><strong>string.format()</strong></p>
</td>
<td>
<p>格式化字符串</p>
</td>
</tr>
<tr>
<td>
<p><strong>string.index(str, beg=0, end=len(string))</strong></p>
</td>
<td>
<p>跟find()方法一样，只不过如果str不在 string中会报一个异常.</p>
</td>
</tr>
<tr>
<td>
<p>string.isalnum()</p>
</td>
<td>
<p>如果 string 至少有一个字符并且所有字符都是字母或数字则返</p>
<p>回 True,否则返回 False</p>
</td>
</tr>
<tr>
<td>
<p>string.isalpha()</p>
</td>
<td>
<p>如果 string 至少有一个字符并且所有字符都是字母则返回 True,</p>
<p>否则返回 False</p>
</td>
</tr>
<tr>
<td>
<p>string.isdecimal()</p>
</td>
<td>
<p>如果 string 只包含十进制数字则返回 True 否则返回 False.</p>
</td>
</tr>
<tr>
<td>
<p>string.isdigit()</p>
</td>
<td>
<p>如果 string 只包含数字则返回 True 否则返回 False.</p>
</td>
</tr>
<tr>
<td>
<p>string.islower()</p>
</td>
<td>
<p>如果 string 中包含至少一个区分大小写的字符，并且所有这些(区分大小写的)字符都是小写，则返回 True，否则返回 False</p>
</td>
</tr>
<tr>
<td>
<p>string.isnumeric()</p>
</td>
<td>
<p>如果 string 中只包含数字字符，则返回 True，否则返回 False</p>
</td>
</tr>
<tr>
<td>
<p>string.isspace()</p>
</td>
<td>
<p>如果 string 中只包含空格，则返回 True，否则返回 False.</p>
</td>
</tr>
<tr>
<td>
<p>string.istitle()</p>
</td>
<td>
<p>如果 string 是标题化的(见 title())则返回 True，否则返回 False</p>
</td>
</tr>
<tr>
<td>
<p>string.isupper()</p>
</td>
<td>
<p>如果 string 中包含至少一个区分大小写的字符，并且所有这些(区分大小写的)字符都是大写，则返回 True，否则返回 False</p>
</td>
</tr>
<tr>
<td>
<p><strong>string.join(seq)</strong></p>
</td>
<td>
<p>以 string 作为分隔符，将 seq 中所有的元素(的字符串表示)合并为一个新的字符串</p>
</td>
</tr>
<tr>
<td>
<p>string.ljust(width)</p>
</td>
<td>
<p>返回一个原字符串左对齐,并使用空格填充至长度 width 的新字符串</p>
</td>
</tr>
<tr>
<td>
<p>string.lower()</p>
</td>
<td>
<p>转换 string 中所有大写字符为小写.</p>
</td>
</tr>
<tr>
<td>
<p>string.lstrip()</p>
</td>
<td>
<p>截掉 string 左边的空格</p>
</td>
</tr>
<tr>
<td>
<p>string.maketrans(intab, outtab])</p>
</td>
<td>
<p> maketrans() 方法用于创建字符映射的转换表，对于接受两个参数的最简单的调用方式，第一个参数是字符串，表示需要转换的字符，第二个参数也是字符串表示转换的目标。</p>
</td>
</tr>
<tr>
<td>
<p>max(str)</p>
</td>
<td>
<p> 返回字符串 <em>str</em> 中最大的字母。</p>
</td>
</tr>
<tr>
<td>
<p>min(str)</p>
</td>
<td>
<p> 返回字符串 <em>str</em> 中最小的字母。</p>
</td>
</tr>
<tr>
<td>
<p><strong>string.partition(str)</strong></p>
</td>
<td>
<p>有点像 find()和 split()的结合体,从 str 出现的第一个位置起,把 字 符 串 string 分 成 一 个 3 元 素 的 元 组 (string_pre_str,str,string_post_str),如果 string 中不包含str 则 string_pre_str == string.</p>
</td>
</tr>
<tr>
<td>
<p><strong>string.replace(str1, str2,&nbsp; num=string.count(str1))</strong></p>
</td>
<td>
<p>把 string 中的 str1 替换成 str2,如果 num 指定，则替换不超过 num 次.</p>
</td>
</tr>
<tr>
<td>
<p>string.rfind(str, beg=0,end=len(string) )</p>
</td>
<td>
<p>类似于 find()函数，不过是从右边开始查找.</p>
</td>
</tr>
<tr>
<td>
<p>string.rindex( str, beg=0,end=len(string))</p>
</td>
<td>
<p>类似于 index()，不过是从右边开始.</p>
</td>
</tr>
<tr>
<td>
<p>string.rjust(width)</p>
</td>
<td>
<p>返回一个原字符串右对齐,并使用空格填充至长度 width 的新字符串</p>
</td>
</tr>
<tr>
<td>
<p>string.rpartition(str)</p>
</td>
<td>
<p>类似于 partition()函数,不过是从右边开始查找</p>
</td>
</tr>
<tr>
<td>
<p>string.rstrip()</p>
</td>
<td>
<p>删除 string 字符串末尾的空格.</p>
</td>
</tr>
<tr>
<td>
<p><strong>string.split(str="", num=string.count(str))</strong></p>
</td>
<td>
<p>以 str 为分隔符切片 string，如果 num有指定值，则仅分隔 num 个子字符串</p>
</td>
</tr>
<tr>
<td>
<p>string.splitlines([keepends])</p>
</td>
<td>
<p>按照行('\r', '\r\n', \n')分隔，返回一个包含各行作为元素的列表，如果参数 keepends 为 False，不包含换行符，如果为 True，则保留换行符。</p>
</td>
</tr>
<tr>
<td>
<p>string.startswith(obj, beg=0,end=len(string))</p>
</td>
<td>
<p>检查字符串是否是以 obj 开头，是则返回 True，否则返回 False。如果beg 和 end 指定值，则在指定范围内检查.</p>
</td>
</tr>
<tr>
<td>
<p><strong>string.strip([obj])</strong></p>
</td>
<td>
<p>在 string 上执行 lstrip()和 rstrip()</p>
</td>
</tr>
<tr>
<td>
<p>string.swapcase()</p>
</td>
<td>
<p>翻转 string 中的大小写</p>
</td>
</tr>
<tr>
<td>
<p>string.title()</p>
</td>
<td>
<p>返回"标题化"的 string,就是说所有单词都是以大写开始，其余字母均为小写(见 istitle())</p>
</td>
</tr>
<tr>
<td>
<p><strong>string.translate(str, del="")</strong></p>
</td>
<td>
<p>根据 str 给出的表(包含 256 个字符)转换 string 的字符,</p>
<p>要过滤掉的字符放到 del 参数中</p>
</td>
</tr>
<tr>
<td>
<p>string.upper()</p>
</td>
<td>
<p>转换 string 中的小写字母为大写</p>
</td>
</tr>
<tr>
<td>
<p>string.zfill(width)</p>
</td>
<td>
<p>返回长度为 width 的字符串，原字符串 string 右对齐，前面填充0</p>
</td>
</tr>
</tbody>
</table>

### Python 列表(List)
序列是Python中最基本的数据结构。序列中的每个元素都分配一个数字 - 它的位置，或索引，第一个索引是0，第二个索引是1，依此类推。

Python有6个序列的内置类型，但最常见的是列表和元组。

序列都可以进行的操作包括索引，切片，加，乘，检查成员。

此外，Python已经内置确定序列的长度以及确定最大和最小的元素的方法。

列表是最常用的Python数据类型，它可以作为一个方括号内的逗号分隔值出现。

列表的数据项不需要具有相同的类型。
#### 更新列表
你可以对列表的数据项进行修改或更新，你也可以使用append()方法来添加列表项，如下所示：
```py
实例(Python 2.0+)
#!/usr/bin/python
# -*- coding: UTF-8 -*-
list = [] ## 空列表
list.append('Google') ## 使用 append() 添加元素
list.append('Runoob')
print list
```
#### 删除列表元素
可以使用 del 语句来删除列表的元素，如下实例：
```py
实例(Python 2.0+)
#!/usr/bin/python
list1 = ['physics', 'chemistry', 1997, 2000]
print list1
del list1[2]
print "After deleting value at index 2 : "
print list1
```
#### Python列表脚本操作符
列表对 + 和 * 的操作符与字符串相似。+ 号用于组合列表，* 号用于重复列表。

如下所示：
<table class="reference">
<tbody><tr>
<th>Python 表达式</th><th>结果 </th><th> 描述</th></tr>
<tr><td>len([1, 2, 3])</td><td>3</td><td>长度</td></tr>
<tr><td>[1, 2, 3] + [4, 5, 6]</td><td>[1, 2, 3, 4, 5, 6]</td><td>组合</td></tr>
<tr><td>['Hi!'] * 4</td><td>['Hi!', 'Hi!', 'Hi!', 'Hi!']</td><td>重复</td></tr>
<tr><td>3 in [1, 2, 3]</td><td>True</td><td>元素是否存在于列表中</td></tr>
<tr><td>for x in [1, 2, 3]: print x,</td><td>1 2 3</td><td>迭代</td></tr>
</tbody></table>

#### Python列表函数&方法
Python包含以下函数:
<table class="reference">
<tbody><tr>
<th style="width:5%">序号</th><th style="width:95%">函数</th></tr>
<tr><td>1</td><td>cmp(list1, list2)<br>比较两个列表的元素</td></tr>
<tr><td>2</td><td>len(list)<br>列表元素个数</td></tr>
<tr><td>3</td><td>max(list)<br>返回列表元素最大值</td></tr>
<tr><td>4</td><td>min(list)<br>返回列表元素最小值</td></tr>
<tr><td>5</td><td>list(seq)<br>将元组转换为列表</td></tr>
</tbody></table>

Python包含以下方法:
<table class="reference">
<tbody><tr>
<th style="width:5%">序号</th><th style="width:95%">方法</th></tr>
<tr><td>1</td><td>list.append(obj)<br>在列表末尾添加新的对象</td></tr>
<tr><td>2</td><td>list.count(obj)<br>统计某个元素在列表中出现的次数</td></tr>
<tr><td>3</td><td>list.extend(seq)<br>在列表末尾一次性追加另一个序列中的多个值（用新列表扩展原来的列表）</td></tr>
<tr><td>4</td><td>list.index(obj)<br>从列表中找出某个值第一个匹配项的索引位置</td></tr>
<tr><td>5</td><td>list.insert(index, obj)<br>将对象插入列表</td></tr>
<tr><td>6</td><td>list.pop([index=-1])<br>移除列表中的一个元素（默认最后一个元素），并且返回该元素的值</td></tr>
<tr><td>7</td><td>list.remove(obj)<br>移除列表中某个值的第一个匹配项</td></tr>
<tr><td>8</td><td>list.reverse()<br>反向列表中元素</td></tr>
<tr><td>9</td><td>list.sort(cmp=None, key=None, reverse=False)<br>对原列表进行排序</td></tr>
</tbody></table>

### Python 元组
Python的元组与列表类似，不同之处在于元组的元素不能修改。

元组使用小括号，列表使用方括号。

元组创建很简单，只需要在括号中添加元素，并使用逗号隔开即可。

如下实例：
```py
tup1 = ('physics', 'chemistry', 1997, 2000);
tup2 = (1, 2, 3, 4, 5 );
tup3 = "a", "b", "c", "d";
```
创建空元组
```py
tup1 = ();
```
元组中只包含一个元素时，需要在元素后面添加逗号
```py
tup1 = (50,);
```
元组与字符串类似，下标索引从0开始，可以进行截取，组合等。
#### 修改元组
元组中的元素值是不允许修改的，但我们可以对元组进行连接组合，如下实例:
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

tup1 = (12, 34.56);
tup2 = ('abc', 'xyz');

# 以下修改元组元素操作是非法的。
# tup1[0] = 100;

# 创建一个新的元组
tup3 = tup1 + tup2;
print tup3;
```
#### 删除元组
元组中的元素值是不允许删除的，但我们可以使用del语句来删除整个元组，如下实例:
```py
#!/usr/bin/python

tup = ('physics', 'chemistry', 1997, 2000);

print tup;
del tup;
print "After deleting tup : "
print tup;
```
#### 元组运算符
与字符串一样，元组之间可以使用 + 号和 * 号进行运算。这就意味着他们可以组合和复制，运算后会生成一个新的元组。
<table class="reference">
<tbody><tr>
<th style="width:33%">Python 表达式</th><th style="width:33%">结果 </th><th style="width:33%"> 描述</th></tr>
<tr><td>len((1, 2, 3))</td><td>3</td><td>计算元素个数</td></tr>
<tr><td>(1, 2, 3) + (4, 5, 6)</td><td>(1, 2, 3, 4, 5, 6)</td><td>连接</td></tr>
<tr><td>('Hi!',) * 4</td><td>('Hi!', 'Hi!', 'Hi!', 'Hi!')</td><td>复制</td></tr>
<tr><td>3 in (1, 2, 3)</td><td>True</td><td>元素是否存在</td></tr>
<tr><td>for x in (1, 2, 3): print x,</td><td>1 2 3</td><td>迭代 </td></tr>
</tbody></table>

#### 无关闭分隔符
任意无符号的对象，以逗号隔开，默认为元组，如下实例：
```py
#!/usr/bin/python

print 'abc', -4.24e93, 18+6.6j, 'xyz';
x, y = 1, 2;
print "Value of x , y : ", x,y;
```
#### 元组内置函数
Python元组包含了以下内置函数
<table class="reference">
<tbody><tr>
<th style="width:5%">序号</th><th style="width:95%">方法及描述</th></tr>
<tr><td>1</td><td>cmp(tuple1, tuple2)<br>比较两个元组元素。</td></tr>
<tr><td>2</td><td>len(tuple)<br>计算元组元素个数。</td></tr>
<tr><td>3</td><td>max(tuple)<br>返回元组中元素最大值。</td></tr>
<tr><td>4</td><td>min(tuple)<br>返回元组中元素最小值。</td></tr>
<tr><td>5</td><td>tuple(seq)<br>将列表转换为元组。</td></tr>
</tbody></table>

### Python 字典(Dictionary)
字典是另一种可变容器模型，且可存储任意类型对象。
字典的每个键值 `key=>value` 对用冒号 `:` 分割，每个键值对之间用逗号 `,` 分割，整个字典包括在花括号 `{}` 中。

键一般是唯一的，如果重复最后的一个键值对会替换前面的，值不需要唯一。

值可以取任何数据类型，但键必须是不可变的，如字符串，数字或元组。

一个简单的字典实例：
```py
dict = {'Alice': '2341', 'Beth': '9102', 'Cecil': '3258'}
```
也可如此创建字典：
```py
dict1 = { 'abc': 456 };
dict2 = { 'abc': 123, 98.6: 37 };
```
#### 访问字典里的值
把相应的键放入熟悉的方括弧。

如果用字典里没有的键访问数据，会输出错误如下：
```py
实例
#!/usr/bin/python
dict = {'Name': 'Zara', 'Age': 7, 'Class': 'First'};
print "dict['Alice']: ", dict['Alice'];
```
以上实例输出结果：
```py
dict['Alice']: 
Traceback (most recent call last):
  File "test.py", line 5, in <module>
    print "dict['Alice']: ", dict['Alice'];
KeyError: 'Alice'
```
#### 修改字典
向字典添加新内容的方法是增加新的键/值对，修改或删除已有键/值对如下实例:

实例
```py
#!/usr/bin/python
dict = {'Name': 'Zara', 'Age': 7, 'Class': 'First'};
dict['Age'] = 8; # update existing entry
dict['School'] = "DPS School"; # Add new entry
print "dict['Age']: ", dict['Age'];
print "dict['School']: ", dict['School'];
```
#### 删除字典元素
能删单一的元素也能清空字典，清空只需一项操作。

显示删除一个字典用del命令，如下实例：
```py
实例
#!/usr/bin/python
# -*- coding: UTF-8 -*-
dict = {'Name': 'Zara', 'Age': 7, 'Class': 'First'};
del dict['Name']; # 删除键是'Name'的条目
dict.clear(); # 清空词典所有条目
del dict ; # 删除词典
print "dict['Age']: ", dict['Age'];
print "dict['School']: ", dict['School'];
```
#### 字典键的特性
字典值可以没有限制地取任何python对象，既可以是标准的对象，也可以是用户定义的，但键不行。

两个重要的点需要记住：

1）不允许同一个键出现两次。创建时如果同一个键被赋值两次，后一个值会被记住，如下实例：
```py
实例
#!/usr/bin/python
dict = {'Name': 'Zara', 'Age': 7, 'Name': 'Manni'};
print "dict['Name']: ", dict['Name'];
```
以上实例输出结果：
```py
dict['Name']:  Manni
```
2）键必须不可变，所以可以用数字，字符串或元组充当，所以用列表就不行，如下实例：
```py
实例
#!/usr/bin/python
dict = {['Name']: 'Zara', 'Age': 7};
print "dict['Name']: ", dict['Name'];
```
以上实例输出结果：
```py
Traceback (most recent call last):
  File "test.py", line 3, in <module>
    dict = {['Name']: 'Zara', 'Age': 7};
TypeError: list objects are unhashable
```
#### 字典内置函数&方法
Python字典包含了以下内置函数：
<table class="reference">
<tbody><tr>
<th>序号</th><th>函数及描述</th></tr>
<tr><td>1</td><td>cmp(dict1, dict2)<br>比较两个字典元素。</td></tr>
<tr><td>2</td><td>len(dict)<br>计算字典元素个数，即键的总数。</td></tr>
<tr><td>3</td><td>str(dict)<br>输出字典可打印的字符串表示。</td></tr>
<tr><td>4</td><td>type(variable)<br>返回输入的变量类型，如果变量是字典就返回字典类型。</td></tr>
</tbody></table>

Python字典包含了以下内置方法：
<table class="reference">
<tbody><tr>
<th style="width:5%">序号</th><th style="width:95%">函数及描述</th></tr>
<tr><td>1</td><td>dict.clear()<br>删除字典内所有元素 </td></tr>
<tr><td>2</td><td>dict.copy()<br>返回一个字典的浅复制</td></tr>
<tr><td>3</td><td>dict.fromkeys(seq[, val])<br> 创建一个新字典，以序列 seq 中元素做字典的键，val 为字典所有键对应的初始值</td></tr>
<tr><td>4</td><td>dict.get(key, default=None)<br>返回指定键的值，如果值不在字典中返回default值</td></tr>
<tr><td>5</td><td>dict.has_key(key)<br>如果键在字典dict里返回true，否则返回false</td></tr>
<tr><td>6</td><td>dict.items()<br>以列表返回可遍历的(键, 值) 元组数组</td></tr>
<tr><td>7</td><td>dict.keys()<br>以列表返回一个字典所有的键</td></tr>
<tr><td>8</td><td>dict.setdefault(key, default=None)<br>
和get()类似, 但如果键不存在于字典中，将会添加键并将值设为default</td></tr>
<tr><td>9</td><td>dict.update(dict2)<br>把字典dict2的键/值对更新到dict里</td></tr>
<tr><td>10</td><td>dict.values()<br>以列表返回字典中的所有值</td></tr>
<tr><td>11</td><td>pop(key[,default])<br>删除字典给定键 key 所对应的值，返回值为被删除的值。key值必须给出。
否则，返回default值。</td></tr>
<tr><td>12</td><td> popitem()<br>随机返回并删除字典中的一对键和值。</td></tr>
</tbody></table>

### Python 日期和时间
Python 程序能用很多方式处理日期和时间，转换日期格式是一个常见的功能。

Python 提供了一个 time 和 calendar 模块可以用于格式化日期和时间。

时间间隔是以秒为单位的浮点小数。

每个时间戳都以自从1970年1月1日午夜（历元）经过了多长时间来表示。

Python 的 time 模块下有很多函数可以转换常见日期格式。如函数time.time()用于获取当前时间戳, 如下实例:
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

import time;  # 引入time模块

ticks = time.time()
print "当前时间戳为:", ticks
```
以上实例输出结果：
```py
当前时间戳为: 1459994552.51
```
时间戳单位最适于做日期运算。但是1970年之前的日期就无法以此表示了。太遥远的日期也不行，UNIX和Windows只支持到2038年。
#### 什么是时间元组？
很多Python函数用一个元组装起来的9组数字处理时间:
<table class="reference">
<tbody><tr>
<th style="width:10%">序号
</th><th style="width:40%">字段</th><th>值</th>
</tr>
<tr><td>0</td><td>4位数年</td><td>2008</td></tr>
<tr><td>1</td><td>月</td><td>1 到 12</td></tr>
<tr><td>2</td><td>日</td><td>1到31</td></tr>
<tr><td>3</td><td>小时</td><td>0到23</td></tr>
<tr><td>4</td><td>分钟</td><td>0到59</td></tr>
<tr><td>5</td><td>秒</td><td>0到61 (60或61 是闰秒)</td></tr>
<tr><td>6</td><td>一周的第几日</td><td>0到6 (0是周一)</td></tr>
<tr><td>7</td><td>一年的第几日</td><td>1到366 (儒略历)</td></tr>
<tr><td>8</td><td>夏令时</td><td>-1, 0, 1, -1是决定是否为夏令时的旗帜</td></tr>
</tbody></table>

上述也就是struct_time元组。这种结构具有如下属性：
<table class="reference">
<tbody><tr>
<th style="width:10%">序号</th><th style="width:40%">属性</th><th>值</th>
</tr>
<tr><td>0</td><td>tm_year</td><td>2008</td></tr>
<tr><td>1</td><td>tm_mon</td><td>1 到 12</td></tr>
<tr><td>2</td><td>tm_mday</td><td>1 到 31</td></tr>
<tr><td>3</td><td>tm_hour</td><td>0 到 23</td></tr>
<tr><td>4</td><td>tm_min</td><td>0 到 59</td></tr>
<tr><td>5</td><td>tm_sec</td><td>0 到 61 (60或61 是闰秒)</td></tr>
<tr><td>6</td><td>tm_wday</td><td>0到6 (0是周一)</td></tr>
<tr><td>7</td><td>tm_yday</td><td>1 到 366(儒略历)</td></tr>
<tr><td>8</td><td>tm_isdst</td><td>-1, 0, 1, -1是决定是否为夏令时的旗帜</td></tr>
</tbody></table>

#### 获取当前时间
从返回浮点数的时间戳方式向时间元组转换，只要将浮点数传递给如localtime之类的函数。
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

import time

localtime = time.localtime(time.time())
print "本地时间为 :", localtime
```
以上实例输出结果：
```py
本地时间为 : time.struct_time(tm_year=2016, tm_mon=4, tm_mday=7, tm_hour=10, tm_min=3, tm_sec=27, tm_wday=3, tm_yday=98, tm_isdst=0)
```
#### 获取格式化的时间
你可以根据需求选取各种格式，但是最简单的获取可读的时间模式的函数是asctime():
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

import time

localtime = time.asctime( time.localtime(time.time()) )
print "本地时间为 :", localtime
```
以上实例输出结果：
```py
本地时间为 : Thu Apr  7 10:05:21 2016
```

#### 格式化日期
我们可以使用 time 模块的 strftime 方法来格式化日期，：
```py
time.strftime(format[, t])
#!/usr/bin/python
# -*- coding: UTF-8 -*-

import time

# 格式化成2016-03-20 11:45:39形式
print time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) 

# 格式化成Sat Mar 28 22:24:24 2016形式
print time.strftime("%a %b %d %H:%M:%S %Y", time.localtime()) 
  
# 将格式字符串转换为时间戳
a = "Sat Mar 28 22:24:24 2016"
print time.mktime(time.strptime(a,"%a %b %d %H:%M:%S %Y"))
```
以上实例输出结果：
```py
2016-04-07 10:25:09
Thu Apr 07 10:25:09 2016
1459175064.0
```
python中时间日期格式化符号：
* %y 两位数的年份表示（00-99）
* %Y 四位数的年份表示（000-9999）
* %m 月份（01-12）
* %d 月内中的一天（0-31）
* %H 24小时制小时数（0-23）
* %I 12小时制小时数（01-12）
* %M 分钟数（00=59）
* %S 秒（00-59）
* %a 本地简化星期名称
* %A 本地完整星期名称
* %b 本地简化的月份名称
* %B 本地完整的月份名称
* %c 本地相应的日期表示和时间表示
* %j 年内的一天（001-366）
* %p 本地A.M.或P.M.的等价符
* %U 一年中的星期数（00-53）星期天为星期的开始
* %w 星期（0-6），星期天为星期的开始
* %W 一年中的星期数（00-53）星期一为星期的开始
* %x 本地相应的日期表示
* %X 本地相应的时间表示
* %Z 当前时区的名称
* %% %号本身
#### 获取某月日历
Calendar模块有很广泛的方法用来处理年历和月历，例如打印某月的月历：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

import calendar

cal = calendar.month(2016, 1)
print "以下输出2016年1月份的日历:"
print cal;
```
以上实例输出结果：
```py
以下输出2016年1月份的日历:
    January 2016
Mo Tu We Th Fr Sa Su
             1  2  3
 4  5  6  7  8  9 10
11 12 13 14 15 16 17
18 19 20 21 22 23 24
25 26 27 28 29 30 31
```
#### Time 模块
Time 模块包含了以下内置函数，既有时间处理的，也有转换时间格式的：
<table class="reference">
<tbody><tr>
<th style="width:5%">序号</th><th style="width:95%">函数及描述</th></tr>
<tr><td>1</td><td>time.altzone<br>返回格林威治西部的夏令时地区的偏移秒数。如果该地区在格林威治东部会返回负值（如西欧，包括英国）。对夏令时启用地区才能使用。</td></tr>
<tr><td>2</td><td>time.asctime([tupletime])<br>接受时间元组并返回一个可读的形式为"Tue Dec 11 18:07:14 2008"（2008年12月11日 周二18时07分14秒）的24个字符的字符串。</td></tr>
<tr><td>3</td><td>time.clock( )<br>用以浮点数计算的秒数返回当前的CPU时间。用来衡量不同程序的耗时，比time.time()更有用。</td></tr>
<tr><td>4</td><td>time.ctime([secs])<br>作用相当于asctime(localtime(secs))，未给参数相当于asctime()</td></tr>
<tr><td>5</td><td>time.gmtime([secs])<br>接收时间戳（1970纪元后经过的浮点秒数）并返回格林威治天文时间下的时间元组t。注：t.tm_isdst始终为0</td></tr>
<tr><td>6</td><td>time.localtime([secs])<br>接收时间戳（1970纪元后经过的浮点秒数）并返回当地时间下的时间元组t（t.tm_isdst可取0或1，取决于当地当时是不是夏令时）。</td></tr>
<tr><td>7</td><td>time.mktime(tupletime)<br>接受时间元组并返回时间戳（1970纪元后经过的浮点秒数）。</td></tr>
<tr><td>8</td><td>time.sleep(secs)<br>推迟调用线程的运行，secs指秒数。</td></tr>
<tr><td>9</td><td>time.strftime(fmt[,tupletime])<br>接收以时间元组，并返回以可读字符串表示的当地时间，格式由fmt决定。</td></tr>
<tr><td>10</td><td>time.strptime(str,fmt='%a %b %d %H:%M:%S %Y')<br>根据fmt的格式把一个时间字符串解析为时间元组。</td></tr>
<tr><td>11</td><td>time.time( )<br>返回当前时间的时间戳（1970纪元后经过的浮点秒数）。</td></tr>
<tr><td>12</td><td>time.tzset()<br>根据环境变量TZ重新初始化时间相关设置。 </td></tr>
</tbody></table>

Time模块包含了以下2个非常重要的属性：
<table class="reference">
<tbody><tr>
<th style="width:5%">序号</th><th style="width:95%">属性及描述</th></tr>
<tr><td>1</td><td><b>time.timezone</b><br>属性time.timezone是当地时区（未启动夏令时）距离格林威治的偏移秒数（>0，美洲;<=0大部分欧洲，亚洲，非洲）。</td></tr>
<tr><td>2</td><td><b>time.tzname</b><br>属性time.tzname包含一对根据情况的不同而不同的字符串，分别是带夏令时的本地时区名称，和不带的。</td></tr>
</tbody></table>

#### 日历（Calendar）模块
此模块的函数都是日历相关的，例如打印某月的字符月历。

星期一是默认的每周第一天，星期天是默认的最后一天。更改设置需调用calendar.setfirstweekday()函数。模块包含了以下内置函数：
<table class="reference">
<tbody><tr>
<th style="width:5%">序号</th><th style="width:95%">函数及描述</th></tr>
<tr><td>1</td><td><b>calendar.calendar(year,w=2,l=1,c=6)</b><br>返回一个多行字符串格式的year年年历，3个月一行，间隔距离为c。 每日宽度间隔为w字符。每行长度为21* W+18+2* C。l是每星期行数。</td></tr>
<tr><td>2</td><td><b>calendar.firstweekday( )</b><br>返回当前每周起始日期的设置。默认情况下，首次载入caendar模块时返回0，即星期一。</td></tr>
<tr><td>3</td><td><b>calendar.isleap(year)</b><br>是闰年返回True，否则为false。</td></tr>
<tr><td>4</td><td><b>calendar.leapdays(y1,y2)</b><br>返回在Y1，Y2两年之间的闰年总数。</td></tr>
<tr><td>5</td><td><b>calendar.month(year,month,w=2,l=1)</b><br>返回一个多行字符串格式的year年month月日历，两行标题，一周一行。每日宽度间隔为w字符。每行的长度为7* w+6。l是每星期的行数。</td></tr>
<tr><td>6</td><td><b>calendar.monthcalendar(year,month)</b><br>返回一个整数的单层嵌套列表。每个子列表装载代表一个星期的整数。Year年month月外的日期都设为0;范围内的日子都由该月第几日表示，从1开始。</td></tr>
<tr><td>7</td><td><b>calendar.monthrange(year,month)</b><br>返回两个整数。第一个是该月的星期几的日期码，第二个是该月的日期码。日从0（星期一）到6（星期日）;月从1到12。</td></tr>
<tr><td>8</td><td><b>calendar.prcal(year,w=2,l=1,c=6)</b><br>相当于 print calendar.calendar(year,w,l,c).</td></tr>
<tr><td>9</td><td><b>calendar.prmonth(year,month,w=2,l=1)</b><br>相当于 print calendar.calendar（year，w，l，c）。</td></tr>
<tr><td>10</td><td><b>calendar.setfirstweekday(weekday)</b><br>设置每周的起始日期码。0（星期一）到6（星期日）。</td></tr>
<tr><td>11</td><td><b>calendar.timegm(tupletime)</b><br>和time.gmtime相反：接受一个时间元组形式，返回该时刻的时间戳（1970纪元后经过的浮点秒数）。</td></tr>
<tr><td>12</td><td><b>calendar.weekday(year,month,day)</b><br>返回给定日期的日期码。0（星期一）到6（星期日）。月份为 1（一月） 到 12（12月）。</td></tr>
</tbody></table>

#### 其他相关模块和函数
在Python中，其他处理日期和时间的模块还有：
* datetime模块
* pytz模块
* dateutil模块

### Python 函数
函数是组织好的，可重复使用的，用来实现单一，或相关联功能的代码段。

函数能提高应用的模块性，和代码的重复利用率。你已经知道Python提供了许多内建函数，比如print()。但你也可以自己创建函数，这被叫做用户自定义函数。

#### 定义一个函数
你可以定义一个由自己想要功能的函数，以下是简单的规则：
* 函数代码块以 def 关键词开头，后接函数标识符名称和圆括号()。
* 任何传入参数和自变量必须放在圆括号中间。圆括号之间可以用于定义参数。
* 函数的第一行语句可以选择性地使用文档字符串—用于存放函数说明。
* 函数内容以冒号起始，并且缩进。
* return [表达式] 结束函数，选择性地返回一个值给调用方。不带表达式的return相当于返回 None。

语法
```py
def functionname( parameters ):
   "函数_文档字符串"
   function_suite
   return [expression]
```
默认情况下，参数值和参数名称是按函数声明中定义的顺序匹配起来的。

#### 函数调用
定义一个函数只给了函数一个名称，指定了函数里包含的参数，和代码块结构。

这个函数的基本结构完成以后，你可以通过另一个函数调用执行，也可以直接从Python提示符执行。
#### 参数传递
在 python 中，类型属于对象，变量是没有类型的：
```py
a=[1,2,3]

a="Runoob"
```
以上代码中，[1,2,3] 是 List 类型，"Runoob" 是 String 类型，而变量 a 是没有类型，她仅仅是一个对象的引用（一个指针），可以是 List 类型对象，也可以指向 String 类型对象。

##### 可更改(mutable)与不可更改(immutable)对象
在 python 中，strings, tuples, 和 numbers 是不可更改的对象，而 list,dict 等则是可以修改的对象。

* 不可变类型：变量赋值 a=5 后再赋值 a=10，这里实际是新生成一个 int 值对象 10，再让 a 指向它，而 5 被丢弃，不是改变a的值，相当于新生成了a。
* 可变类型：变量赋值 la=[1,2,3,4] 后再赋值 la[2]=5 则是将 list la 的第三个元素值更改，本身la没有动，只是其内部的一部分值被修改了。

##### python 函数的参数传递：
* 不可变类型：类似 c++ 的值传递，如 整数、字符串、元组。如fun（a），传递的只是a的值，没有影响a对象本身。比如在 fun（a）内部修改 a 的值，只是修改另一个复制的对象，不会影响 a 本身。
* 可变类型：类似 c++ 的引用传递，如 列表，字典。如 fun（la），则是将 la 真正的传过去，修改后fun外部的la也会受影响

python 中一切都是对象，严格意义我们不能说值传递还是引用传递，我们应该说传不可变对象和传可变对象。

python 传不可变对象实例
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
def ChangeInt( a ):
    a = 10

b = 2
ChangeInt(b)
print b # 结果是 2
```
实例中有 int 对象 2，指向它的变量是 b，在传递给 ChangeInt 函数时，按传值的方式复制了变量 b，a 和 b 都指向了同一个 Int 对象，在 a=10 时，则新生成一个 int 值对象 10，并让 a 指向它。

传可变对象实例
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
# 可写函数说明
def changeme( mylist ):
   "修改传入的列表"
   mylist.append([1,2,3,4]);
   print "函数内取值: ", mylist
   return
 
# 调用changeme函数
mylist = [10,20,30];
changeme( mylist );
print "函数外取值: ", mylist
```
实例中传入函数的和在末尾添加新内容的对象用的是同一个引用，故输出结果如下：
```py
函数内取值:  [10, 20, 30, [1, 2, 3, 4]]
函数外取值:  [10, 20, 30, [1, 2, 3, 4]]
```
#### 参数
以下是调用函数时可使用的正式参数类型：
* 必备参数
* 关键字参数
* 默认参数
* 不定长参数
##### 必备参数
必备参数须以正确的顺序传入函数。调用时的数量必须和声明时的一样。

调用printme()函数，你必须传入一个参数，不然会出现语法错误：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
#可写函数说明
def printme( str ):
   "打印任何传入的字符串"
   print str;
   return;
 
#调用printme函数
printme();
```
##### 关键字参数
关键字参数和函数调用关系紧密，函数调用使用关键字参数来确定传入的参数值。

使用关键字参数允许函数调用时参数的顺序与声明时不一致，因为 Python 解释器能够用参数名匹配参数值。

以下实例在函数 printme() 调用时使用参数名：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
#可写函数说明
def printme( str ):
   "打印任何传入的字符串"
   print str;
   return;
 
#调用printme函数
printme( str = "My string");
```
##### 缺省参数
调用函数时，缺省参数的值如果没有传入，则被认为是默认值。下例会打印默认的age，如果age没有被传入：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
#可写函数说明
def printinfo( name, age = 35 ):
   "打印任何传入的字符串"
   print "Name: ", name;
   print "Age ", age;
   return;
 
#调用printinfo函数
printinfo( age=50, name="miki" );
printinfo( name="miki" );
```
##### 不定长参数
你可能需要一个函数能处理比当初声明时更多的参数。这些参数叫做不定长参数，和上述2种参数不同，声明时不会命名。基本语法如下：
```py
def functionname([formal_args,] *var_args_tuple ):
   "函数_文档字符串"
   function_suite
   return [expression]
```
加了星号（*）的变量名会存放所有未命名的变量参数。不定长参数实例如下：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
# 可写函数说明
def printinfo( arg1, *vartuple ):
   "打印任何传入的参数"
   print "输出: "
   print arg1
   for var in vartuple:
      print var
   return;
 
# 调用printinfo 函数
printinfo( 10 );
printinfo( 70, 60, 50 );
```
以上实例输出结果：
```py
输出:
10
输出:
70
60
50
```
##### 匿名函数
python 使用 lambda 来创建匿名函数。

lambda只是一个表达式，函数体比def简单很多。

lambda的主体是一个表达式，而不是一个代码块。仅仅能在lambda表达式中封装有限的逻辑进去。

lambda函数拥有自己的命名空间，且不能访问自有参数列表之外或全局命名空间里的参数。

虽然lambda函数看起来只能写一行，却不等同于C或C++的内联函数，后者的目的是调用小函数时不占用栈内存从而增加运行效率。

语法

lambda函数的语法只包含一个语句，如下：
```py
lambda [arg1 [,arg2,.....argn]]:expression
```
如下实例：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
# 可写函数说明
sum = lambda arg1, arg2: arg1 + arg2;
 
# 调用sum函数
print "相加后的值为 : ", sum( 10, 20 )
print "相加后的值为 : ", sum( 20, 20 )
```
##### return 语句
return语句[表达式]退出函数，选择性地向调用方返回一个表达式。不带参数值的return语句返回None。之前的例子都没有示范如何返回数值，下例便告诉你怎么做：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
# 可写函数说明
def sum( arg1, arg2 ):
   # 返回2个参数的和."
   total = arg1 + arg2
   print "函数内 : ", total
   return total;
 
# 调用sum函数
total = sum( 10, 20 );
```
#### 变量作用域
一个程序的所有的变量并不是在哪个位置都可以访问的。访问权限决定于这个变量是在哪里赋值的。

变量的作用域决定了在哪一部分程序你可以访问哪个特定的变量名称。两种最基本的变量作用域如下：
* 全局变量
* 局部变量
#### 全局变量和局部变量
定义在函数内部的变量拥有一个局部作用域，定义在函数外的拥有全局作用域。

局部变量只能在其被声明的函数内部访问，而全局变量可以在整个程序范围内访问。调用函数时，所有在函数内声明的

变量名称都将被加入到作用域中。如下实例：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

total = 0; # 这是一个全局变量
# 可写函数说明
def sum( arg1, arg2 ):
   #返回2个参数的和."
   total = arg1 + arg2; # total在这里是局部变量.
   print "函数内是局部变量 : ", total
   return total;
 
#调用sum函数
sum( 10, 20 );
print "函数外是全局变量 : ", total
```
### Python 模块
Python 模块(Module)，是一个 Python 文件，以 `.py` 结尾，包含了 Python 对象定义和Python语句。

模块让你能够有逻辑地组织你的 Python 代码段。

把相关的代码分配到一个模块里能让你的代码更好用，更易懂。

模块能定义函数，类和变量，模块里也能包含可执行的代码。

例子

下例是个简单的模块 `support.py`：

`support.py` 模块：
```py
def print_func( par ):
print "Hello : ", par
return
```
#### import 语句
模块的引入

模块定义好后，我们可以使用 import 语句来引入模块，语法如下：
```py
import module1[, module2[,... moduleN]
```
比如要引用模块 math，就可以在文件最开始的地方用 import math 来引入。在调用 math 模块中的函数时，必须这样引用：
```py
模块名.函数名
```
当解释器遇到 import 语句，如果模块在当前的搜索路径就会被导入。

搜索路径是一个解释器会先进行搜索的所有目录的列表。如想要导入模块 support.py，需要把命令放在脚本的顶端：

test.py 文件代码：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
# 导入模块
import support
# 现在可以调用模块里包含的函数了
support.print_func("Runoob")
```
以上实例输出结果：
```py
Hello : Runoob
```
一个模块只会被导入一次，不管你执行了多少次import。这样可以防止导入模块被一遍又一遍地执行。
#### from…import 语句
Python 的 from 语句让你从模块中导入一个指定的部分到当前命名空间中。语法如下：
```py
from modname import name1[, name2[, ... nameN]]
```
例如，要导入模块 fib 的 fibonacci 函数，使用如下语句：
```py
from fib import fibonacci
```
这个声明不会把整个 fib 模块导入到当前的命名空间中，它只会将 fib 里的 fibonacci 单个引入到执行这个声明的模块的全局符号表。
#### from…import* 语句
把一个模块的所有内容全都导入到当前的命名空间也是可行的，只需使用如下声明：
```py
from modname import *
```
这提供了一个简单的方法来导入一个模块中的所有项目。然而这种声明不该被过多地使用。

例如我们想一次性引入 math 模块中所有的东西，语句如下：
```py
from math import *
```
#### 搜索路径
当你导入一个模块，Python 解析器对模块位置的搜索顺序是：
* 1、当前目录
* 2、如果不在当前目录，Python 则搜索在 shell 变量 PYTHONPATH 下的每个目录。
* 3、如果都找不到，Python会察看默认路径。UNIX下，默认路径一般为/usr/local/lib/python/。
模块搜索路径存储在 system 模块的 sys.path 变量中。变量里包含当前目录，PYTHONPATH和由安装过程决定的默认目录。
#### PYTHONPATH 变量
作为环境变量，PYTHONPATH 由装在一个列表里的许多目录组成。PYTHONPATH 的语法和 shell 变量 PATH 的一样。

在 Windows 系统，典型的 PYTHONPATH 如下：
```
set PYTHONPATH=c:\python27\lib;
```
在 UNIX 系统，典型的 PYTHONPATH 如下：
```
set PYTHONPATH=/usr/local/lib/python
```
#### 命名空间和作用域
变量是拥有匹配对象的名字（标识符）。命名空间是一个包含了变量名称们（键）和它们各自相应的对象们（值）的字典。

一个 Python 表达式可以访问局部命名空间和全局命名空间里的变量。如果一个局部变量和一个全局变量重名，则局部变量会覆盖全局变量。

每个函数都有自己的命名空间。类的方法的作用域规则和通常函数的一样。

Python 会智能地猜测一个变量是局部的还是全局的，它假设任何在函数内赋值的变量都是局部的。

因此，如果要给函数内的全局变量赋值，必须使用 global 语句。

global VarName 的表达式会告诉 Python， VarName 是一个全局变量，这样 Python 就不会在局部命名空间里寻找这个变量了。

例如，我们在全局命名空间里定义一个变量 Money。我们再在函数内给变量 Money 赋值，然后 Python 会假定 Money 是一个局部变量。然而，我们并没有在访问前声明一个局部变量 Money，结果就是会出现一个 UnboundLocalError 的错误。取消 global 语句的注释就能解决这个问题。
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
Money = 2000
def AddMoney():
   # 想改正代码就取消以下注释:
   # global Money
   Money = Money + 1
 
print Money
AddMoney()
print Money
```
#### dir()函数
dir() 函数一个排好序的字符串列表，内容是一个模块里定义过的名字。

返回的列表容纳了在一个模块里定义的所有模块，变量和函数。如下一个简单的实例：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
# 导入内置math模块
import math
 
content = dir(math)
 
print content;
```
以上实例输出结果：
```py
['__doc__', '__file__', '__name__', 'acos', 'asin', 'atan', 
'atan2', 'ceil', 'cos', 'cosh', 'degrees', 'e', 'exp', 
'fabs', 'floor', 'fmod', 'frexp', 'hypot', 'ldexp', 'log',
'log10', 'modf', 'pi', 'pow', 'radians', 'sin', 'sinh', 
'sqrt', 'tan', 'tanh']
```
在这里，特殊字符串变量__name__指向模块的名字，__file__指向该模块的导入文件名。

#### globals() 和 locals() 函数
根据调用地方的不同，globals() 和 locals() 函数可被用来返回全局和局部命名空间里的名字。

如果在函数内部调用 locals()，返回的是所有能在该函数里访问的命名。

如果在函数内部调用 globals()，返回的是所有在该函数里能访问的全局名字。

两个函数的返回类型都是字典。所以名字们能用 keys() 函数摘取。

#### reload() 函数
当一个模块被导入到一个脚本，模块顶层部分的代码只会被执行一次。

因此，如果你想重新执行模块里顶层部分的代码，可以用 reload() 函数。该函数会重新导入之前导入过的模块。语法

如下：
```py
reload(module_name)
```
在这里，module_name要直接放模块的名字，而不是一个字符串形式。比如想重载 hello 模块，如下：
```py
reload(hello)
```
#### Python中的包
包是一个分层次的文件目录结构，它定义了一个由模块及子包，和子包下的子包等组成的 Python 的应用环境。

简单来说，包就是文件夹，但该文件夹下必须存在 \_\_init\_\_.py 文件, 该文件的内容可以为空。\_\_init\_\_.py 用于标识当前文件夹是一个包。

考虑一个在 package_runoob 目录下的 runoob1.py、runoob2.py、\_\_init\_\_.py 文件，test.py 为测试调用包的代码，目录结构如下：
```py
test.py
package_runoob
|-- __init__.py
|-- runoob1.py
|-- runoob2.py 
```
源代码如下：
```py
package_runoob/runoob1.py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
def runoob1():
print "I'm in runoob1"
package_runoob/runoob2.py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
def runoob2():
print "I'm in runoob2"
```
现在，在 package_runoob 目录下创建 \_\_init\_\_.py：
```py
package_runoob/__init__.py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
if __name__ == '__main__':
print '作为主程序运行'
else:
print 'package_runoob 初始化'
```
然后我们在 package_runoob 同级目录下创建 test.py 来调用 package_runoob 包
```py
test.py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
# 导入 Phone 包
from package_runoob.runoob1 import runoob1
from package_runoob.runoob2 import runoob2
runoob1()
runoob2()
```
以上实例输出结果：
```py
package_runoob 初始化
I'm in runoob1
I'm in runoob2
```
如上，为了举例，我们只在每个文件里放置了一个函数，但其实你可以放置许多函数。你也可以在这些文件里定义Python的类，然后为这些类建一个包。
### Python 文件I/O
本章只讲述所有基本的的I/O函数，更多函数请参考Python标准文档。

#### 打印到屏幕

最简单的输出方法是用print语句，你可以给它传递零个或多个用逗号隔开的表达式。此函数把你传递的表达式转换成一个字符串表达式，并将结果写到标准输出如下：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*- 

print "Python 是一个非常棒的语言，不是吗？"
```
你的标准屏幕上会产生以下结果：
```py
Python 是一个非常棒的语言，不是吗？
```
#### 读取键盘输入
Python提供了两个内置函数从标准输入读入一行文本，默认的标准输入是键盘。如下：
* raw_input
* input
##### raw_input函数
```py
raw_input([prompt]) 函数从标准输入读取一个行，并返回一个字符串（去掉结尾的换行符）：
#!/usr/bin/python
# -*- coding: UTF-8 -*- 
 
str = raw_input("请输入：")
print "你输入的内容是: ", str
```
这将提示你输入任意字符串，然后在屏幕上显示相同的字符串。当我输入"Hello Python！"，它的输出如下：
```py
请输入：Hello Python！
你输入的内容是:  Hello Python！
```
##### input函数
input([prompt]) 函数和 raw_input([prompt]) 函数基本类似，但是 input 可以接收一个Python表达式作为输入，并将运算结果返回。
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*- 
 
str = input("请输入：")
print "你输入的内容是: ", str
```
这会产生如下的对应着输入的结果：
```py
请输入：[x*5 for x in range(2,10,2)]
你输入的内容是:  [10, 20, 30, 40]
```
#### 打开和关闭文件
现在，您已经可以向标准输入和输出进行读写。现在，来看看怎么读写实际的数据文件。

Python 提供了必要的函数和方法进行默认情况下的文件基本操作。你可以用 file 对象做大部分的文件操作。
#### open 函数
你必须先用Python内置的open()函数打开一个文件，创建一个file对象，相关的方法才可以调用它进行读写。
语法：
```py
file object = open(file_name [, access_mode][, buffering])
```
各个参数的细节如下：
* file_name：file_name变量是一个包含了你要访问的文件名称的字符串值。
* access_mode：access_mode决定了打开文件的模式：只读，写入，追加等。所有可取值见如下的完全列表。这个参数是非强制的，默认文件访问模式为只读(r)。
* buffering:如果buffering的值被设为0，就不会有寄存。如果buffering的值取1，访问文件时会寄存行。如果将buffering的值设为大于1的整数，表明了这就是的寄存区的缓冲大小。如果取负值，寄存区的缓冲大小则为系统默认。

不同模式打开文件的完全列表：
<table class="reference">
<tbody><tr><th style="width:10%">模式</th><th>描述</th></tr>
<tr><td>r</td><td>以只读方式打开文件。文件的指针将会放在文件的开头。这是默认模式。</td></tr>
<tr><td>rb</td><td>以二进制格式打开一个文件用于只读。文件指针将会放在文件的开头。这是默认模式。一般用于非文本文件如图片等。</td></tr>
<tr><td>r+</td><td>打开一个文件用于读写。文件指针将会放在文件的开头。</td></tr>
<tr><td>rb+</td><td>以二进制格式打开一个文件用于读写。文件指针将会放在文件的开头。一般用于非文本文件如图片等。</td></tr>
<tr><td>w</td><td>打开一个文件只用于写入。如果该文件已存在则打开文件，并从开头开始编辑，即原有内容会被删除。如果该文件不存在，创建新文件。</td></tr>
<tr><td>wb</td><td>以二进制格式打开一个文件只用于写入。如果该文件已存在则打开文件，并从开头开始编辑，即原有内容会被删除。如果该文件不存在，创建新文件。一般用于非文本文件如图片等。</td></tr>
<tr><td>w+</td><td>打开一个文件用于读写。如果该文件已存在则打开文件，并从开头开始编辑，即原有内容会被删除。如果该文件不存在，创建新文件。</td></tr>
<tr><td>wb+</td><td>以二进制格式打开一个文件用于读写。如果该文件已存在则打开文件，并从开头开始编辑，即原有内容会被删除。如果该文件不存在，创建新文件。一般用于非文本文件如图片等。</td></tr>
<tr><td>a</td><td>打开一个文件用于追加。如果该文件已存在，文件指针将会放在文件的结尾。也就是说，新的内容将会被写入到已有内容之后。如果该文件不存在，创建新文件进行写入。</td></tr>
<tr><td>ab</td><td>以二进制格式打开一个文件用于追加。如果该文件已存在，文件指针将会放在文件的结尾。也就是说，新的内容将会被写入到已有内容之后。如果该文件不存在，创建新文件进行写入。</td></tr>
<tr><td>a+</td><td>打开一个文件用于读写。如果该文件已存在，文件指针将会放在文件的结尾。文件打开时会是追加模式。如果该文件不存在，创建新文件用于读写。</td></tr>
<tr><td>ab+</td><td>以二进制格式打开一个文件用于追加。如果该文件已存在，文件指针将会放在文件的结尾。如果该文件不存在，创建新文件用于读写。</td></tr>
</tbody></table>

<table class="reference">
<thead>
<tr>
<th style="text-align:center">模式</th>
<th style="text-align:center">r</th>
<th style="text-align:center">r+</th>
<th style="text-align:center">w</th>
<th style="text-align:center">w+</th>
<th style="text-align:center">a</th>
<th style="text-align:center">a+</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:center">读</td>
<td style="text-align:center">+</td>
<td style="text-align:center">+</td>
<td style="text-align:center"></td>
<td style="text-align:center">+</td>
<td style="text-align:center"></td>
<td style="text-align:center">+</td>
</tr>
<tr>
<td style="text-align:center">写</td>
<td style="text-align:center"></td>
<td style="text-align:center">+</td>
<td style="text-align:center">+</td>
<td style="text-align:center">+</td>
<td style="text-align:center">+</td>
<td style="text-align:center">+</td>
</tr>
<tr>
<td style="text-align:center">创建</td>
<td style="text-align:center"></td>
<td style="text-align:center"></td>
<td style="text-align:center">+</td>
<td style="text-align:center">+</td>
<td style="text-align:center">+</td>
<td style="text-align:center">+</td>
</tr>
<tr>
<td style="text-align:center">覆盖</td>
<td style="text-align:center"></td>
<td style="text-align:center"></td>
<td style="text-align:center">+</td>
<td style="text-align:center">+</td>
<td style="text-align:center"></td>
<td style="text-align:center"></td>
</tr>
<tr>
<td style="text-align:center">指针在开始</td>
<td style="text-align:center">+</td>
<td style="text-align:center">+</td>
<td style="text-align:center">+</td>
<td style="text-align:center">+</td>
<td style="text-align:center"></td>
<td style="text-align:center"></td>
</tr>
<tr>
<td style="text-align:center">指针在结尾</td>
<td style="text-align:center"></td>
<td style="text-align:center"></td>
<td style="text-align:center"></td>
<td style="text-align:center"></td>
<td style="text-align:center">+</td>
<td style="text-align:center">+</td>
</tr>
</tbody>
</table>

#### File对象的属性
一个文件被打开后，你有一个file对象，你可以得到有关该文件的各种信息。

以下是和file对象相关的所有属性的列表：
<table class="reference">
<tbody><tr><th>属性</th><th>描述</th></tr>
<tr><td>file.closed</td><td>返回true如果文件已被关闭，否则返回false。</td></tr>
<tr><td>file.mode</td><td>返回被打开文件的访问模式。</td></tr>
<tr><td>file.name</td><td>返回文件的名称。</td></tr>
<tr><td>file.softspace</td><td>如果用print输出后，必须跟一个空格符，则返回false。否则返回true。</td></tr>
</tbody></table>

如下实例：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
# 打开一个文件
fo = open("foo.txt", "w")
print "文件名: ", fo.name
print "是否已关闭 : ", fo.closed
print "访问模式 : ", fo.mode
print "末尾是否强制加空格 : ", fo.softspace
```
以上实例输出结果：
```py
文件名:  foo.txt
是否已关闭 :  False
访问模式 :  w
末尾是否强制加空格 :  0
```
##### close()方法
File 对象的 close（）方法刷新缓冲区里任何还没写入的信息，并关闭该文件，这之后便不能再进行写入。

当一个文件对象的引用被重新指定给另一个文件时，Python 会关闭之前的文件。用 close（）方法关闭文件是一个很好的习惯。
语法：
```py
fileObject.close()
```
例子：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
# 打开一个文件
fo = open("foo.txt", "w")
print "文件名: ", fo.name
 
# 关闭打开的文件
fo.close()
```
以上实例输出结果：
```py
文件名:  foo.txt
```
读写文件：

file对象提供了一系列方法，能让我们的文件访问更轻松。来看看如何使用read()和write()方法来读取和写入文件。
##### write()方法
write()方法可将任何字符串写入一个打开的文件。需要重点注意的是，Python字符串可以是二进制数据，而不是仅仅是文字。

write()方法不会在字符串的结尾添加换行符('\n')：
语法：
```py
fileObject.write(string)
```
在这里，被传递的参数是要写入到已打开文件的内容。
例子：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
# 打开一个文件
fo = open("foo.txt", "w")
fo.write( "www.runoob.com!\nVery good site!\n")
 
# 关闭打开的文件
fo.close()
```
上述方法会创建foo.txt文件，并将收到的内容写入该文件，并最终关闭文件。如果你打开这个文件，将看到以下内容:
```py
$ cat foo.txt 
www.runoob.com!
Very good site!
```
##### read()方法
read（）方法从一个打开的文件中读取一个字符串。需要重点注意的是，Python字符串可以是二进制数据，而不是仅仅是文字。
语法：
```py
fileObject.read([count])
```
在这里，被传递的参数是要从已打开文件中读取的字节计数。该方法从文件的开头开始读入，如果没有传入count，它会尝试尽可能多地读取更多的内容，很可能是直到文件的末尾。
例子：

这里我们用到以上创建的 foo.txt 文件。
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
# 打开一个文件
fo = open("foo.txt", "r+")
str = fo.read(10)
print "读取的字符串是 : ", str
# 关闭打开的文件
fo.close()
```
以上实例输出结果：
```py
读取的字符串是 :  www.runoob
```

#### 文件定位
tell()方法告诉你文件内的当前位置, 换句话说，下一次的读写会发生在文件开头这么多字节之后。

seek（offset [,from]）方法改变当前文件的位置。Offset变量表示要移动的字节数。From变量指定开始移动字节的参考位置。

如果from被设为0，这意味着将文件的开头作为移动字节的参考位置。如果设为1，则使用当前的位置作为参考位置。如果它被设为2，那么该文件的末尾将作为参考位置。

例子：

就用我们上面创建的文件foo.txt。
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-
 
# 打开一个文件
fo = open("foo.txt", "r+")
str = fo.read(10)
print "读取的字符串是 : ", str
 
# 查找当前位置
position = fo.tell()
print "当前文件位置 : ", position
 
# 把指针再次重新定位到文件开头
position = fo.seek(0, 0)
str = fo.read(10)
print "重新读取字符串 : ", str
# 关闭打开的文件
fo.close()
```
以上实例输出结果：
```py
读取的字符串是 :  www.runoob
当前文件位置 :  10
重新读取字符串 :  www.runoob
```
#### 重命名和删除文件
Python的os模块提供了帮你执行文件处理操作的方法，比如重命名和删除文件。

要使用这个模块，你必须先导入它，然后才可以调用相关的各种功能。
##### rename()方法：
rename()方法需要两个参数，当前的文件名和新文件名。
语法：
```py
os.rename(current_file_name, new_file_name)
```
例子：

下例将重命名一个已经存在的文件test1.txt。
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

import os
 
# 重命名文件test1.txt到test2.txt。
os.rename( "test1.txt", "test2.txt" )
```
##### remove()方法
你可以用remove()方法删除文件，需要提供要删除的文件名作为参数。
语法：
```py
os.remove(file_name)
```
例子：

下例将删除一个已经存在的文件test2.txt。
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

import os
 
# 删除一个已经存在的文件test2.txt
os.remove("test2.txt")
```
#### Python里的目录：
所有文件都包含在各个不同的目录下，不过Python也能轻松处理。os模块有许多方法能帮你创建，删除和更改目录。
##### mkdir()方法
可以使用os模块的mkdir()方法在当前目录下创建新的目录们。你需要提供一个包含了要创建的目录名称的参数。
语法：
```py
os.mkdir("newdir")
```
例子：

下例将在当前目录下创建一个新目录test。
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

import os
 
# 创建目录test
os.mkdir("test")
```
##### chdir()方法
可以用chdir()方法来改变当前的目录。chdir()方法需要的一个参数是你想设成当前目录的目录名称。
语法：
```py
os.chdir("newdir")
```
例子：

下例将进入"/home/newdir"目录。
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

import os
 
# 将当前目录改为"/home/newdir"
os.chdir("/home/newdir")
```
##### getcwd()方法：
getcwd()方法显示当前的工作目录。
语法：
```py
os.getcwd()
```
例子：

下例给出当前目录：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

import os
 
# 给出当前的目录
print os.getcwd()
```
##### rmdir()方法
rmdir()方法删除目录，目录名称以参数传递。

在删除这个目录之前，它的所有内容应该先被清除。
语法：
```py
os.rmdir('dirname')
```
例子：

以下是删除" /tmp/test"目录的例子。目录的完全合规的名称必须被给出，否则会在当前目录下搜索该目录。
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

import os
 
# 删除”/tmp/test”目录
os.rmdir( "/tmp/test"  )
```
### Python File(文件) 方法
file 对象使用 open 函数来创建，下表列出了 file 对象常用的函数：
<table class="reference">
<tbody><tr>
<th style="width:5%">序号</th><th>方法及描述</th></tr>
<tr><td>1</td><td><p>file.close()</p><p>关闭文件。关闭后文件不能再进行读写操作。</p></td></tr>
<tr><td>2</td><td><p>file.flush()</p><p>刷新文件内部缓冲，直接把内部缓冲区的数据立刻写入文件, 而不是被动的等待输出缓冲区写入。</p></td></tr>
<tr><td>3</td><td><p>file.fileno()</p><p>
返回一个整型的文件描述符(file descriptor FD 整型), 可以用在如os模块的read方法等一些底层操作上。</p></td></tr>
<tr><td>4</td><td><p>file.isatty()</p><p>如果文件连接到一个终端设备返回 True，否则返回 False。</p></td></tr>
<tr><td>5</td><td><p>file.next()</p><p>返回文件下一行。</p></td></tr>
<tr><td>6</td><td><p>file.read([size])</p><p>从文件读取指定的字节数，如果未给定或为负则读取所有。</p></td></tr>
<tr><td>7</td><td><p>file.readline([size])</p><p>读取整行，包括 "\n" 字符。</p></td></tr>
<tr><td>8</td><td><p>file.readlines([sizehint])</p><p>读取所有行并返回列表，若给定sizeint>0，则是设置一次读多少字节，这是为了减轻读取压力。</p></td></tr>
<tr><td>9</td><td><p>file.seek(offset[, whence])</p><p>设置文件当前位置</p></td></tr>
<tr><td>10</td><td><p>file.tell()</p><p>返回文件当前位置。</p></td></tr>
<tr><td>11</td><td><p>file.truncate([size])</p><p>截取文件，截取的字节通过size指定，默认为当前文件位置。 </p></td></tr>
<tr><td>12</td><td><p>file.write(str)</p><p>将字符串写入文件，返回的是写入的字符长度。</p></td></tr>
<tr><td>13</td><td><p>file.writelines(sequence)</p><p>向文件写入一个序列字符串列表，如果需要换行则要自己加入每行的换行符。</p></td></tr>
</tbody></table>

### Python 异常处理
python提供了两个非常重要的功能来处理python程序在运行中出现的异常和错误。你可以使用该功能来调试python程序。
* 异常处理
* 断言(Assertions)
#### python标准异常
<table class="reference">
<tr>
<th>
异常名称</th><th> 描述</th>
<tr>
<tr><td>
BaseException</td><td> 所有异常的基类</td></tr><tr><td>
SystemExit</td><td>解释器请求退出</td></tr><tr><td>
KeyboardInterrupt</td><td> 用户中断执行(通常是输入^C)</td></tr><tr><td>
Exception</td><td>常规错误的基类</td></tr><tr><td>
StopIteration </td><td>迭代器没有更多的值</td></tr><tr><td>
GeneratorExit </td><td>生成器(generator)发生异常来通知退出</td></tr><tr><td>
StandardError</td><td> 所有的内建标准异常的基类</td></tr><tr><td>
ArithmeticError </td><td>所有数值计算错误的基类</td></tr><tr><td>
FloatingPointError </td><td>浮点计算错误</td></tr><tr><td>
OverflowError</td><td> 数值运算超出最大限制</td></tr><tr><td>
ZeroDivisionError</td><td> 除(或取模)零 (所有数据类型)</td></tr><tr><td>
AssertionError</td><td> 断言语句失败</td></tr><tr><td>
AttributeError</td><td> 对象没有这个属性</td></tr><tr><td>
EOFError </td><td>没有内建输入,到达EOF 标记</td></tr><tr><td>
EnvironmentError </td><td>操作系统错误的基类</td></tr><tr><td>
IOError </td><td>输入/输出操作失败</td></tr><tr><td>
OSError </td><td>操作系统错误</td></tr><tr><td>
WindowsError</td><td> 系统调用失败</td></tr><tr><td>
ImportError </td><td>导入模块/对象失败</td></tr><tr><td>
LookupError </td><td>无效数据查询的基类</td></tr><tr><td>
IndexError</td><td> 序列中没有此索引(index)</td></tr><tr><td>
KeyError</td><td> 映射中没有这个键</td></tr><tr><td>
MemoryError</td><td> 内存溢出错误(对于Python 解释器不是致命的)</td></tr><tr><td>
NameError</td><td> 未声明/初始化对象 (没有属性)</td></tr><tr><td>
UnboundLocalError </td><td>访问未初始化的本地变量</td></tr><tr><td>
ReferenceError</td><td> 弱引用(Weak reference)试图访问已经垃圾回收了的对象</td></tr><tr><td>
RuntimeError </td><td>一般的运行时错误</td></tr><tr><td>
NotImplementedError</td><td> 尚未实现的方法</td></tr><tr><td>
SyntaxError</td><td>Python 语法错误</td></tr><tr><td>
IndentationError</td><td> 缩进错误</td></tr><tr><td>
TabError</td><td> Tab 和空格混用</td></tr><tr><td>
SystemError </td><td>一般的解释器系统错误</td></tr><tr><td>
TypeError</td><td> 对类型无效的操作</td></tr><tr><td>
ValueError </td><td>传入无效的参数</td></tr><tr><td>
UnicodeError </td><td>Unicode 相关的错误</td></tr><tr><td>
UnicodeDecodeError </td><td>Unicode 解码时的错误</td></tr><tr><td>
UnicodeEncodeError </td><td>Unicode 编码时错误</td></tr><tr><td>
UnicodeTranslateError</td><td> Unicode 转换时错误</td></tr><tr><td>
Warning </td><td>警告的基类</td></tr><tr><td>
DeprecationWarning</td><td> 关于被弃用的特征的警告</td></tr><tr><td>
FutureWarning </td><td>关于构造将来语义会有改变的警告</td></tr><tr><td>
OverflowWarning</td><td> 旧的关于自动提升为长整型(long)的警告</td></tr><tr><td>
PendingDeprecationWarning</td><td> 关于特性将会被废弃的警告</td></tr><tr><td>
RuntimeWarning </td><td>可疑的运行时行为(runtime behavior)的警告</td></tr><tr><td>
SyntaxWarning</td><td> 可疑的语法的警告</td></tr><tr><td>
UserWarning</td><td> 用户代码生成的警告</td></tr></table>

#### 什么是异常？
异常即是一个事件，该事件会在程序执行过程中发生，影响了程序的正常执行。

一般情况下，在Python无法正常处理程序时就会发生一个异常。

异常是Python对象，表示一个错误。

当Python脚本发生异常时我们需要捕获处理它，否则程序会终止执行。
#### 异常处理
捕捉异常可以使用try/except语句。

try/except语句用来检测try语句块中的错误，从而让except语句捕获异常信息并处理。

如果你不想在异常发生时结束你的程序，只需在try里捕获它。

语法：

以下为简单的try....except...else的语法：
```py
try:
<语句>        #运行别的代码
except <名字>：
<语句>        #如果在try部份引发了'name'异常
except <名字>，<数据>:
<语句>        #如果引发了'name'异常，获得附加的数据
else:
<语句>        #如果没有异常发生
```
try的工作原理是，当开始一个try语句后，python就在当前程序的上下文中作标记，这样当异常出现时就可以回到这里，try子句先执行，接下来会发生什么依赖于执行时是否出现异常。

如果当try后的语句执行时发生异常，python就跳回到try并执行第一个匹配该异常的except子句，异常处理完毕，控制流就通过整个try语句（除非在处理异常时又引发新的异常）。

如果在try后的语句里发生了异常，却没有匹配的except子句，异常将被递交到上层的try，或者到程序的最上层（这样将结束程序，并打印缺省的出错信息）。

如果在try子句执行时没有发生异常，python将执行else语句后的语句（如果有else的话），然后控制流通过整个try语句。
##### 实例
下面是简单的例子，它打开一个文件，在该文件中的内容写入内容，且并未发生异常：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

try:
    fh = open("testfile", "w")
    fh.write("这是一个测试文件，用于测试异常!!")
except IOError:
    print "Error: 没有找到文件或读取文件失败"
else:
    print "内容写入文件成功"
    fh.close()
```
以上程序输出结果：
```py
$ python test.py 
内容写入文件成功
$ cat testfile       # 查看写入的内容
这是一个测试文件，用于测试异常!!
```
##### 实例
下面是简单的例子，它打开一个文件，在该文件中的内容写入内容，但文件没有写入权限，发生了异常：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

try:
    fh = open("testfile", "w")
    fh.write("这是一个测试文件，用于测试异常!!")
except IOError:
    print "Error: 没有找到文件或读取文件失败"
else:
    print "内容写入文件成功"
    fh.close()
```
在执行代码前为了测试方便，我们可以先去掉 testfile 文件的写权限，命令如下：
```py
chmod -w testfile
```
再执行以上代码：
```py
$ python test.py 
Error: 没有找到文件或读取文件失败
```
#### 使用except而不带任何异常类型
你可以不带任何异常类型使用except，如下实例：
```py
try:
    正常的操作
   ......................
except:
    发生异常，执行这块代码
   ......................
else:
    如果没有异常执行这块代码
```
以上方式try-except语句捕获所有发生的异常。但这不是一个很好的方式，我们不能通过该程序识别出具体的异常信息。因为它捕获所有的异常。
#### 使用except而带多种异常类型
你也可以使用相同的except语句来处理多个异常信息，如下所示：
```py
try:
    正常的操作
   ......................
except(Exception1[, Exception2[,...ExceptionN]]]):
   发生以上多个异常中的一个，执行这块代码
   ......................
else:
    如果没有异常执行这块代码
```
#### try-finally 语句
try-finally 语句无论是否发生异常都将执行最后的代码。
```py
try:
<语句>
finally:
<语句>    #退出try时总会执行
raise
```
实例
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

try:
    fh = open("testfile", "w")
    fh.write("这是一个测试文件，用于测试异常!!")
finally:
    print "Error: 没有找到文件或读取文件失败"
```
如果打开的文件没有可写权限，输出如下所示：
```py
$ python test.py 
Error: 没有找到文件或读取文件失败
```
同样的例子也可以写成如下方式：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

try:
    fh = open("testfile", "w")
    try:
        fh.write("这是一个测试文件，用于测试异常!!")
    finally:
        print "关闭文件"
        fh.close()
except IOError:
    print "Error: 没有找到文件或读取文件失败"
```
当在try块中抛出一个异常，立即执行finally块代码。

finally块中的所有语句执行后，异常被再次触发，并执行except块代码。

参数的内容不同于异常。
#### 异常的参数
一个异常可以带上参数，可作为输出的异常信息参数。

你可以通过except语句来捕获异常的参数，如下所示：
```py
try:
    正常的操作
   ......................
except ExceptionType, Argument:
    你可以在这输出 Argument 的值...
```
变量接收的异常值通常包含在异常的语句中。在元组的表单中变量可以接收一个或者多个值。

元组通常包含错误字符串，错误数字，错误位置。

##### 实例

以下为单个异常的实例：
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

# 定义函数
def temp_convert(var):
    try:
        return int(var)
    except ValueError, Argument:
        print "参数没有包含数字\n", Argument

# 调用函数
temp_convert("xyz");
```
以上程序执行结果如下：
```py
$ python test.py 
参数没有包含数字
invalid literal for int() with base 10: 'xyz'
```
##### 触发异常
我们可以使用raise语句自己触发异常

raise语法格式如下：
```py
raise [Exception [, args [, traceback]]]
```
语句中 Exception 是异常的类型（例如，NameError）参数标准异常中任一种，args 是自已提供的异常参数。

最后一个参数是可选的（在实践中很少使用），如果存在，是跟踪异常对象。
##### 实例
一个异常可以是一个字符串，类或对象。 Python的内核提供的异常，大多数都是实例化的类，这是一个类的实例的参数。

定义一个异常非常简单，如下所示：
```py
def functionName( level ):
    if level < 1:
        raise Exception("Invalid level!", level)
        # 触发异常后，后面的代码就不会再执行
```
注意：为了能够捕获异常，"except"语句必须有用相同的异常来抛出类对象或者字符串。

例如我们捕获以上异常，"except"语句如下所示：
```py
try:
    正常逻辑
except Exception,err:
    触发自定义异常    
else:
    其余代码
```
##### 实例
```py
#!/usr/bin/python
# -*- coding: UTF-8 -*-

# 定义函数
def mye( level ):
    if level < 1:
        raise Exception,"Invalid level!"
        # 触发异常后，后面的代码就不会再执行
try:
    mye(0)            # 触发异常
except Exception,err:
    print 1,err
else:
    print 2
```
执行以上代码，输出结果为：
```py
$ python test.py 
1 Invalid level!
```
#### 用户自定义异常
通过创建一个新的异常类，程序可以命名它们自己的异常。异常应该是典型的继承自Exception类，通过直接或间接的方式。

以下为与RuntimeError相关的实例,实例中创建了一个类，基类为RuntimeError，用于在异常触发时输出更多的信息。

在try语句块中，用户自定义的异常后执行except块语句，变量 e 是用于创建Networkerror类的实例。
```py
class Networkerror(RuntimeError):
    def __init__(self, arg):
        self.args = arg
```
在你定义以上类后，你可以触发该异常，如下所示：
```py
try:
    raise Networkerror("Bad hostname")
except Networkerror,e:
    print e.args
```
### Python OS 文件/目录方法
os 模块提供了非常丰富的方法用来处理文件和目录。常用的方法如下表所示：
<table class="reference">
<tbody><tr>
<th style="width:5%">序号</th><th>方法及描述</th></tr>
<tr><td>1</td><td><p>os.access(path, mode)</p><br>检验权限模式 </td></tr>
<tr><td>2</td><td><p>os.chdir(path)</p><br>改变当前工作目录</td></tr>
<tr><td>3</td><td><p>os.chflags(path, flags)</p><br>设置路径的标记为数字标记。</td></tr>
<tr><td>4</td><td><p>os.chmod(path, mode)</p><br>更改权限 </td></tr>
<tr><td>5</td><td><p>os.chown(path, uid, gid)</p><br>更改文件所有者</td></tr>
<tr><td>6</td><td><p>os.chroot(path)</p><br>改变当前进程的根目录 </td></tr>
<tr><td>7</td><td><p>os.close(fd)</p><br>关闭文件描述符 fd</td></tr>
<tr><td>8</td><td><p>os.closerange(fd_low, fd_high)</p><br>关闭所有文件描述符，从 fd_low (包含) 到 fd_high (不包含), 错误会忽略</td></tr>
<tr><td>9</td><td><p>os.dup(fd)</p><br>复制文件描述符 fd</td></tr>
<tr><td>10</td><td><p>os.dup2(fd, fd2)</p><br>将一个文件描述符 fd 复制到另一个 fd2</td></tr>
<tr><td>11</td><td><p>os.fchdir(fd)</p><br>通过文件描述符改变当前工作目录</td></tr>
<tr><td>12</td><td><p>os.fchmod(fd, mode)</p><br>改变一个文件的访问权限，该文件由参数fd指定，参数mode是Unix下的文件访问权限。</td></tr>
<tr><td>13</td><td><p>os.fchown(fd, uid, gid)</p><br>修改一个文件的所有权，这个函数修改一个文件的用户ID和用户组ID，该文件由文件描述符fd指定。 </td></tr>
<tr><td>14</td><td><p>os.fdatasync(fd)</p><br>强制将文件写入磁盘，该文件由文件描述符fd指定，但是不强制更新文件的状态信息。</td></tr>
<tr><td>15</td><td><p>os.fdopen(fd[, mode[, bufsize]])</p><br>通过文件描述符 fd 创建一个文件对象，并返回这个文件对象 </td></tr>
<tr><td>16</td><td><p>os.fpathconf(fd, name)</p><br>返回一个打开的文件的系统配置信息。name为检索的系统配置的值，它也许是一个定义系统值的字符串，这些名字在很多标准中指定（POSIX.1, Unix 95, Unix 98, 和其它）。</td></tr>
<tr><td>17</td><td><p>os.fstat(fd)</p><br>返回文件描述符fd的状态，像stat()。 </td></tr>
<tr><td>18</td><td><p>os.fstatvfs(fd)</p><br>返回包含文件描述符fd的文件的文件系统的信息，像 statvfs()</td></tr>
<tr><td>19</td><td><p>os.fsync(fd)</p><br>强制将文件描述符为fd的文件写入硬盘。</td></tr>
<tr><td>20</td><td><p>os.ftruncate(fd, length)</p><br>裁剪文件描述符fd对应的文件, 所以它最大不能超过文件大小。</td></tr>
<tr><td>21</td><td><p>os.getcwd()</p><br>返回当前工作目录 </td></tr>
<tr><td>22</td><td><p>os.getcwdu()</p><br>返回一个当前工作目录的Unicode对象</td></tr>
<tr><td>23</td><td><p>os.isatty(fd)</p><br>如果文件描述符fd是打开的，同时与tty(-like)设备相连，则返回true, 否则False。</td></tr>
<tr><td>24</td><td><p>os.lchflags(path, flags)</p><br>设置路径的标记为数字标记，类似 chflags()，但是没有软链接</td></tr>
<tr><td>25</td><td><p>os.lchmod(path, mode)</p><br>修改连接文件权限</td></tr>
<tr><td>26</td><td><p>os.lchown(path, uid, gid)</p><br>更改文件所有者，类似 chown，但是不追踪链接。 </td></tr>
<tr><td>27</td><td><p>os.link(src, dst)</p><br>创建硬链接，名为参数 dst，指向参数 src</td></tr>
<tr><td>28</td><td><p>os.listdir(path)</p><br>返回path指定的文件夹包含的文件或文件夹的名字的列表。 </td></tr>
<tr><td>29</td><td><p>os.lseek(fd, pos, how)</p><br>设置文件描述符 fd当前位置为pos, how方式修改: SEEK_SET 或者 0 设置从文件开始的计算的pos; SEEK_CUR或者 1 则从当前位置计算; os.SEEK_END或者2则从文件尾部开始. 在unix，Windows中有效</td></tr>
<tr><td>30</td><td><p>os.lstat(path)</p><br>像stat(),但是没有软链接 </td></tr>
<tr><td>31</td><td><p>os.major(device)</p><br>从原始的设备号中提取设备major号码 (使用stat中的st_dev或者st_rdev field)。</td></tr>
<tr><td>32</td><td><p>os.makedev(major, minor)</p><br>以major和minor设备号组成一个原始设备号</td></tr>
<tr><td>33</td><td><p>os.makedirs(path[, mode])</p><br>递归文件夹创建函数。像mkdir(), 但创建的所有intermediate-level文件夹需要包含子文件夹。</td></tr>
<tr><td>34</td><td><p>os.minor(device)</p><br>从原始的设备号中提取设备minor号码 (使用stat中的st_dev或者st_rdev field )。</td></tr>
<tr><td>35</td><td><p>os.mkdir(path[, mode])</p><br>以数字mode的mode创建一个名为path的文件夹.默认的 mode 是 0777 (八进制)。 </td></tr>
<tr><td>36</td><td><p>os.mkfifo(path[, mode])</p><br>创建命名管道，mode 为数字，默认为 0666 (八进制) </td></tr>
<tr><td>37</td><td><p>os.mknod(filename[, mode=0600, device])<br>创建一个名为filename文件系统节点（文件，设备特别文件或者命名pipe）。</p></td></tr>
<tr><td>38</td><td><p>os.open(file, flags[, mode])</p><br>打开一个文件，并且设置需要的打开选项，mode参数是可选的</td></tr>
<tr><td>39</td><td><p>os.openpty()</p><br>打开一个新的伪终端对。返回 pty 和 tty的文件描述符。</td></tr>
<tr><td>40</td><td><p>os.pathconf(path, name)</p><br>
返回相关文件的系统配置信息。 </td></tr>
<tr><td>41</td><td><p>os.pipe()</p><br>创建一个管道. 返回一对文件描述符(r, w) 分别为读和写</td></tr>
<tr><td>42</td><td><p>os.popen(command[, mode[, bufsize]])</p><br>从一个 command 打开一个管道</td></tr>
<tr><td>43</td><td><p>os.read(fd, n)</p><br>从文件描述符 fd 中读取最多 n 个字节，返回包含读取字节的字符串，文件描述符 fd对应文件已达到结尾, 返回一个空字符串。</td></tr>
<tr><td>44</td><td><p>os.readlink(path)</p><br>返回软链接所指向的文件 </td></tr>
<tr><td>45</td><td><p>os.remove(path)</p><br>删除路径为path的文件。如果path 是一个文件夹，将抛出OSError; 查看下面的rmdir()删除一个 directory。 </td></tr>
<tr><td>46</td><td><p>os.removedirs(path)</p><br>递归删除目录。</td></tr>
<tr><td>47</td><td><p>os.rename(src, dst)</p><br>重命名文件或目录，从 src 到 dst</td></tr>
<tr><td>48</td><td><p>os.renames(old, new)</p><br>递归地对目录进行更名，也可以对文件进行更名。</td></tr>
<tr><td>49</td><td><p>os.rmdir(path)</p><br>删除path指定的空目录，如果目录非空，则抛出一个OSError异常。</td></tr>
<tr><td>50</td><td><p>os.stat(path)</p><br>获取path指定的路径的信息，功能等同于C API中的stat()系统调用。</td></tr>
<tr><td>51</td><td><p>os.stat_float_times([newvalue])<br>决定stat_result是否以float对象显示时间戳</p></td></tr>
<tr><td>52</td><td><p>os.statvfs(path)</p><br>获取指定路径的文件系统统计信息</td></tr>
<tr><td>53</td><td><p>os.symlink(src, dst)</p><br>创建一个软链接</td></tr>
<tr><td>54</td><td><p>os.tcgetpgrp(fd)</p><br>返回与终端fd（一个由os.open()返回的打开的文件描述符）关联的进程组 </td></tr>
<tr><td>55</td><td><p>os.tcsetpgrp(fd, pg)</p><br>设置与终端fd（一个由os.open()返回的打开的文件描述符）关联的进程组为pg。</td></tr>
<tr><td>56</td><td><p>os.tempnam([dir[, prefix]])</p><br>返回唯一的路径名用于创建临时文件。 </td></tr>
<tr><td>57</td><td><p>os.tmpfile()</p><br>返回一个打开的模式为(w+b)的文件对象 .这文件对象没有文件夹入口，没有文件描述符，将会自动删除。 </td></tr>
<tr><td>58</td><td><p>os.tmpnam()</p><br>为创建一个临时文件返回一个唯一的路径</td></tr>
<tr><td>59</td><td><p>os.ttyname(fd)</p><br>返回一个字符串，它表示与文件描述符fd 关联的终端设备。如果fd 没有与终端设备关联，则引发一个异常。</td></tr>
<tr><td>60</td><td><p>os.unlink(path)</p><br>删除文件路径 </td></tr>
<tr><td>61</td><td><p>os.utime(path, times)</p><br>返回指定的path文件的访问和修改的时间。 </td></tr>
<tr><td>62</td><td><p>os.walk(top[, topdown=True[, onerror=None[, followlinks=False]]])</p><br>输出在文件夹中的文件名通过在树中游走，向上或者向下。</td></tr>
<tr><td>63</td><td><p>os.write(fd, str)</p><br>写入字符串到文件描述符 fd中. 返回实际写入的字符串长度</td></tr>
</tbody></table>

### Python 内置函数
<table class="reference">
<thead valign="bottom">
<tr>
<th></th>
<th></th>
<th>内置函数</th>
<th></th>
<th></th>
</tr>
</thead>
<tbody valign="top">
<tr class="row-even">
<td>abs()</td>
<td>divmod()</td>
<td>input()</td>
<td>open()</td>
<td>staticmethod()</td>
</tr>
<tr>
<td>all()</td>
<td>enumerate()</td>
<td>int()</td>
<td>ord()</td>
<td>str()</td>
</tr>
<tr class="row-even">
<td>any()</td>
<td>eval()</td>
<td>isinstance()</td>
<td>pow()</td>
<td>sum()</td>
</tr>
<tr>
<td>basestring()</td>
<td>execfile()</td>
<td>issubclass()</td>
<td>print()</td>
<td>super()</td>
</tr>
<tr>
<td>bin()</td>
<td>file()</td>
<td>iter()</td>
<td>property()</td>
<td>tuple()</td>
</tr>
<tr>
<td>bool()</td>
<td>filter()</td>
<td>len()</td>
<td>range()</td>
<td>type()</td>
</tr>
<tr>
<td>bytearray()</td>
<td>float()</td>
<td>list()</td>
<td>raw_input()</td>
<td>unichr()</td>
</tr>
<tr>
<td>callable()</td>
<td>format()</td>
<td>locals()</td>
<td>reduce()</td>
<td>unicode()</td>
</tr>
<tr class="row-even">
<td>chr()</td>
<td>frozenset()</td>
<td>long()</td>
<td>reload()</td>
<td>vars()</td>
</tr>
<tr>
<td>classmethod()</td>
<td>getattr()</td>
<td>map()</td>
<td>repr()</td>
<td>xrange()</td>
</tr>
<tr class="row-even">
<td>cmp()</td>
<td>globals()</td>
<td>max()</td>
<td>reverse()</td>
<td>zip()</td>
</tr>
<tr>
<td>compile()</td>
<td>hasattr()</td>
<td>memoryview()</td>
<td>round()</td>
<td>__import__()</td>
</tr>
<tr>
<td>complex()</td>
<td>hash()</td>
<td>min()</td>
<td>set()</td>
<td></td>
</tr>
<tr>
<td>delattr()</td>
<td>help()</td>
<td>next()</td>
<td>setattr()</td>
<td></td>
</tr>
<tr>
<td>dict()</td>
<td>hex()</td>
<td>object()</td>
<td>slice()</td>
<td></td>
</tr>
<tr>
<td>dir()</td>
<td>id()</td>
<td>oct()</td>
<td>sorted()</td>
<td>exec 内置表达式</td>
</tr>
</tbody>
</table>

===================python基础部分的笔记就到这里========================









